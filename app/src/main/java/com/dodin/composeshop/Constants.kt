package com.dodin.composeshop

object Constants {
    const val PHONE_LENGTH = 10 // 0660037681
    const val WEBVIEW_MOBILE_PARAM = "?type=mobile"
}