package com.dodin.composeshop.di

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import ua.brander.core.platform.NetworkHandler

fun provideSharedPreferences(context: Context): SharedPreferences =
    context.getSharedPreferences("shop_app_parent_preferences", Activity.MODE_PRIVATE)

fun provideNetworkHandler(context: Context): NetworkHandler = NetworkHandler(context)