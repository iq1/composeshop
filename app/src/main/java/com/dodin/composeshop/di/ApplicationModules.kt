package com.dodin.composeshop.di

import com.dodin.composeshop.core.base.NavigationManager
import com.dodin.composeshop.data.network.ApiWorker
import com.dodin.composeshop.data.network.repository.HomeScreenRepository
import com.dodin.composeshop.data.network.repository.ProductRepository
import com.dodin.composeshop.data.network.repository.RegistrationRepository
import com.dodin.composeshop.data.storage.AppPreferences
import com.dodin.composeshop.presentation.authorization.UserAuthorizationViewModel
import com.dodin.composeshop.presentation.main.home.HomeViewModel
import com.dodin.composeshop.presentation.onboarding.OnBoardingViewModel
import com.dodin.composeshop.presentation.product.ProductViewModel
import com.google.gson.Gson
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {
    viewModel { OnBoardingViewModel() }
    viewModel { UserAuthorizationViewModel() }
    viewModel { HomeViewModel() }
    viewModel { parameters -> ProductViewModel(parameters.get()) }
}

val repositoryModule = module {
    single { RegistrationRepository(get(), get(), get()) }
    single { HomeScreenRepository(get(), get(), get()) }
    single { ProductRepository(get(), get(), get()) }
}

val netWorkModule = module {
    single { provideNetworkHandler(androidContext()) }
    single { provideClient() }
    factory { ApiWorker(get(), get(), get()) }
}

val applicationModule = module {
    single { provideSharedPreferences(androidContext()) }
    single { AppPreferences(get()) }
    single { Gson() }
    single { NavigationManager() }
}