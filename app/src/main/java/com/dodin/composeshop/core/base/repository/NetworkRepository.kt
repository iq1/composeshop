package com.dodin.composeshop.core.base.repository

import com.dodin.composeshop.data.network.models.BaseResponse
import ua.brander.core.exception.Failure
import ua.brander.core.functional.Either
import timber.log.Timber

interface NetworkRepository {

    abstract fun isConnected() : Boolean

    companion object {
        const val ERROR_MESSAGE = "message"
    }

    suspend fun <R> networkRequest(requestsBlock: () -> BaseResponse<R>): Either<Failure, R> {
        return if (isConnected()) {
            try {
                val b = requestsBlock()
                if (b.errors.isNullOrEmpty()) Either.Right(b.data!!)
                else Either.Left(Failure.ServerErrorWithDescription(0, b.errors.joinToString { it[ERROR_MESSAGE].toString() }))
            } catch (ex: Exception) {
                if (isConnected()) {
                    Timber.e(ex)
                    Either.Left(Failure.ServerError())
                } else {
                    Either.Left(Failure.NetworkConnection())
                }
            }
        } else Either.Left(Failure.NetworkConnection())
    }

}