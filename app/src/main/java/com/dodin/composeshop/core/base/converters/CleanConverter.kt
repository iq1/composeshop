package com.dodin.composeshop.core.base.converters

interface CleanConverter<E, N, P> {
    fun convertFromEntity(entity : E) : P
    fun convertFromNetworkResponse(response : N) : P
}

interface NetworkConverter<N, P> {
    fun convertFromNetworkResponse(response : N) : P
    fun convertFromNetworkResponse(response : List<N>) : List<P> = response.map { convertFromNetworkResponse(it) }
}

interface DbConverter<E, P> {
    fun convertFromEntity(entity : E) : P
}