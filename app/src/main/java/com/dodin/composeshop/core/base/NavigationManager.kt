package com.dodin.composeshop.core.base

import kotlinx.coroutines.channels.Channel

class NavigationManager {
    val navCommand: Channel<NavigationCommand> = Channel()

    suspend fun navigate(route: Route) {
        navCommand.send(NavigationCommand.NavigateTo(route))
    }

    suspend fun popBackStack(route: Route? = null, inclusive: Boolean = true) {
        navCommand.send(NavigationCommand.PopBackStack(route, inclusive))
    }
}

interface Route {
    val destination: String
}

sealed class NavigationCommand {
    class NavigateTo(val route: Route) : NavigationCommand()
    class PopBackStack(val route: Route? = null, val inclusive: Boolean = true) : NavigationCommand()
}