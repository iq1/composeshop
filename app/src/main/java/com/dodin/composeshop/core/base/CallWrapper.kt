package com.dodin.composeshop.core.base

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.Call
import okhttp3.Response
import okhttp3.ResponseBody

/**
 * Typed wrapper for OkHttp call
 */
class CallWrapper<T : Any>(
    private val call: Call,
    private val typeToken: TypeToken<T>,
) {

    private var body: Response? = null

    private fun body(): ResponseBody? {
        if (body == null) {
            body = call.execute()
        }
        return body?.body
    }

    fun execute(gson: Gson): T {
        return gson.fromJson(body()?.string(), typeToken.type)
    }

    companion object {
        inline operator fun <reified T : Any> invoke(call: Call) =
            CallWrapper(call, object : TypeToken<T>() {})
    }
}