package com.dodin.composeshop

import com.dodin.composeshop.data.Const

object ShopConfig {
    const val baseUrl = "https://shop-app.m2.branderstudio.com/"
    const val confidelityPath = "konfidencialnost"
    val onBoardingAlignment = OnBoardingAlignment.LEFT

    const val confidelityUrl = baseUrl + confidelityPath
    const val graphQlUrl = baseUrl + Const.BASE_ENDPOINT
}

enum class OnBoardingAlignment {
    CENTER, LEFT
}