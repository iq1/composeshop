package com.dodin.composeshop.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import com.dodin.composeshop.theme.Typography

@Composable
fun Collapsable(
    title: String,
    titleStyle: TextStyle,
    modifier: Modifier = Modifier,
    isOpen: Boolean = true,
    content: @Composable () -> Unit,
) {
    var state by remember { mutableStateOf(isOpen) }

    Column(modifier = modifier) {
        Row(modifier = Modifier.clickable { state = !state }) {
            Text(
                text = title,
                style = titleStyle,
            )
            Spacer(Modifier.weight(1f))
            Icon(
                imageVector = if (state) Icons.Default.KeyboardArrowUp
                else Icons.Default.KeyboardArrowDown,
                contentDescription = null
            )
        }
        if (state) content()
    }
}

@Preview
@Composable
fun CollapsableOpenPreview() {
    Collapsable(
        title = "Hello",
        titleStyle = Typography.h2,
        modifier = Modifier.fillMaxWidth(),
        isOpen = true,
        content = { Text("Content") },
    )
}

@Preview
@Composable
fun CollapsableClosedPreview() {
    Collapsable(
        title = "Hello",
        titleStyle = Typography.h2,
        modifier = Modifier.fillMaxWidth(),
        isOpen = false,
        content = { Text("Content") },
    )
}