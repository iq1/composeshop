package com.dodin.composeshop.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dodin.composeshop.R
import com.dodin.composeshop.theme.Gray900
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import ua.brander.core.exception.Failure

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ModalBottomErrorLayout(
    failure: Failure?,
    coroutineScope: CoroutineScope,
    sheetState: ModalBottomSheetState =
        rememberModalBottomSheetState(ModalBottomSheetValue.Hidden),
    content: @Composable () -> Unit
) {
    ModalBottomSheetLayout(
        sheetState = sheetState,
        sheetContent = {
            BottomSheetError(failure) {
                coroutineScope.launch { sheetState.hide() }
            }
        },
        sheetShape = RoundedCornerShape(topStart = 32.dp, topEnd = 32.dp),
        content = content,
    )
}

@Composable
fun BottomSheetError(failure: Failure?, onOkClick: () -> Unit) {
    when (failure) {
        is Failure.ServerErrorWithDescription -> ErrorContent(
            message = failure.message,
            onOkClick = onOkClick
        )
        is Failure.NetworkConnection -> ErrorContent(
            title = stringResource(R.string.no_connection),
            onOkClick = onOkClick
        )
        is Failure.NoDataError -> {} // TODO
        is Failure.ServerError -> {} // TODO
        null -> Spacer(Modifier.height(1.dp)) // TODO: how to avoid it?
    }
}

@Composable
private fun ErrorContent(
    message: String? = null,
    title: String? = null,
    icon: Int? = null,
    onOkClick: () -> Unit,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth(),
    ) {
        Spacer(Modifier.height(8.dp))
        Box(
            Modifier
                .size(60.dp, 4.dp)
                .background(Gray900.copy(alpha = 0.2f))
                .padding(bottom = 8.dp)
        )

        Image(
            painter = painterResource(icon ?: R.drawable.custom_shop_error_dialog_image),
            contentDescription = null,
            modifier = Modifier.padding(top = 32.dp, bottom = 40.dp)
        )
        Text(
            text = title ?: stringResource(R.string.error_title),
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.h1,
            modifier = Modifier.padding(bottom = 8.dp)
        )
        if (message != null) Text(
            text = message,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            fontSize = 14.sp,
            modifier = Modifier.padding(horizontal = 16.dp)
        )

        ShopAppPrimaryButton(
            text = stringResource(R.string.ok_text),
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 24.dp, vertical = 20.dp),
            onClick = onOkClick
        )
    }
}