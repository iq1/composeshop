package com.dodin.composeshop.components

import androidx.compose.foundation.layout.height
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import com.dodin.composeshop.theme.Black30alpha

@Composable
fun ShopAppPrimaryButton(
    text: String,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    onClick: () -> Unit
) {
    Button(
        onClick = onClick,
        enabled = enabled,
        modifier = modifier.height(60.dp),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color.Black,
            disabledBackgroundColor = Black30alpha
        ),
        shape = MaterialTheme.shapes.medium,
    ) {
        Text(text = text, color = Color.White)
    }
}

@Composable
fun ShopAppSecondaryButton(
    text: String,
    modifier: Modifier = Modifier,
    icon: (@Composable () -> Unit)? = null,
    onClick: () -> Unit
) {
    OutlinedButton(
        onClick = onClick,
        modifier = modifier.height(60.dp),
        colors = ButtonDefaults.buttonColors(backgroundColor = Color.White),
        shape = MaterialTheme.shapes.medium,
    ) {
        icon?.invoke()
        Text(text = text, color = Color.Black)
    }
}

@Preview
@Composable
fun PrimaryButtonPreview(@PreviewParameter(provider = ParamsProvider::class) enabled: Boolean) {
    ShopAppPrimaryButton(text = "Button", enabled = enabled) { }
}

@Preview
@Composable
fun SecondaryButtonPreview() {
    ShopAppSecondaryButton(text = "Button") { }
}

class ParamsProvider : PreviewParameterProvider<Boolean> {
    override val values: Sequence<Boolean> = sequenceOf(true, false)
}