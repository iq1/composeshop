package com.dodin.composeshop.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dodin.composeshop.presentation.main.home.mockNewProducts
import com.dodin.composeshop.presentation.models.product.ProductLabelModel

@Composable
fun ProductLabelsList(labels: List<ProductLabelModel>, modifier: Modifier = Modifier) {
    Column(modifier = modifier, verticalArrangement = Arrangement.spacedBy(2.dp)) {
        labels.forEach { label ->
            Surface(
                shape = RoundedCornerShape(topEnd = 6.dp, bottomEnd = 6.dp, bottomStart = 6.dp),
                color = Color(android.graphics.Color.parseColor(label.hexColor))
            ) {
                Text(
                    text = label.title,
                    fontSize = 11.sp,
                    color = Color.White,
                    modifier = Modifier.padding(top = 2.dp, bottom = 2.dp, start = 8.dp, end = 8.dp)
                )
            }
        }
    }
}

@Preview
@Composable
fun ProductLabelsListPreview() {
    ProductLabelsList(mockNewProducts.first().labels.orEmpty())
}