package com.dodin.composeshop.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material.icons.outlined.Share
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.dodin.composeshop.R
import com.dodin.composeshop.theme.ColorPrimaryDisable
import com.dodin.composeshop.theme.Gray950
import com.dodin.composeshop.theme.GrayRampLightenest
import com.dodin.composeshop.theme.PressedYellow

@Composable
fun CloseButton(modifier: Modifier = Modifier, onClick: () -> Unit) {
    IconButton(
        onClick = onClick,
        modifier = modifier.size(24.dp)
    ) {
        Icon(imageVector = Icons.Default.Clear, contentDescription = null)
    }
}

@Composable
fun BackButton(modifier: Modifier = Modifier, onClick: () -> Unit) {
    IconButton(
        onClick = onClick,
        modifier = modifier.size(24.dp)
    ) {
        Icon(imageVector = Icons.Default.KeyboardArrowLeft, contentDescription = null)
    }
}

@Composable
fun ShareButton(modifier: Modifier = Modifier, onClick: () -> Unit) {
    IconButton(
        onClick = onClick,
        modifier = modifier.size(24.dp)
    ) {
        Icon(imageVector = Icons.Outlined.Share, contentDescription = null)
    }
}

@Composable
fun FavoriteButton(
    isFavorite: Boolean,
    modifier: Modifier = Modifier,
    tint: Color = Color.Black,
    onClick: () -> Unit
) {
    IconButton(
        onClick = onClick,
        modifier = modifier.size(24.dp)
    ) {
        FavoriteIcon(isFavorite, tint)
    }
}

@Composable
fun FavoriteIcon(isFavorite: Boolean, tint: Color) {
    Icon(
        imageVector = if (isFavorite) Icons.Filled.Favorite else Icons.Outlined.FavoriteBorder,
        tint = tint,
        contentDescription = null,
    )
}

@Composable
fun ProductCircleCartButton(
    isInCart: Boolean,
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    IconButton(
        onClick = onClick,
        modifier = modifier
            .background(
                color = if (isInCart) PressedYellow else GrayRampLightenest,
                shape = CircleShape
            )
            .size(40.dp, 40.dp)
    ) {
        Icon(
            painter = painterResource(R.drawable.custom_shop_product_button_cart),
            tint = if (isInCart) Gray950 else ColorPrimaryDisable,
            contentDescription = null,
        )
    }
}

@Preview
@Composable
fun IconButtonsPreview() {
    Column {
        Row {
            CloseButton(onClick = {})
            BackButton(onClick = {})
        }
        Row {
            FavoriteButton(true, onClick = {})
            FavoriteButton(false, onClick = {})
        }
        Row {
            ProductCircleCartButton(true, onClick = {})
            ProductCircleCartButton(false, onClick = {})
        }
        ShareButton {}
    }
}