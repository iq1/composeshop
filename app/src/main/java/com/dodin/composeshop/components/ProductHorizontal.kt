package com.dodin.composeshop.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.dodin.composeshop.presentation.main.home.mockPopularProducts
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.dodin.composeshop.theme.FavoriteYellow
import com.dodin.composeshop.theme.GrayRampDarkestLight

@Composable
fun ProductHorizontal(
    product: ProductModel,
    onProductClick: (ProductModel) -> Unit,
    onFavoriteClick: (ProductModel) -> Unit,
    onCartClick: (ProductModel) -> Unit,
) {
    Box(modifier = Modifier
        .clickable { onProductClick(product) }
    ) {
        Row(
            modifier = Modifier.fillMaxWidth().height(135.dp),
        ) {
            Image(
                painter = rememberImagePainter(product.image),
                contentDescription = product.name,
                modifier = Modifier
                    .size(135.dp)
                    .padding(start = 16.dp, top = 16.dp, bottom = 16.dp),
            )

            Column(modifier = Modifier.fillMaxHeight()) {
                Text(
                    text = product.name.orEmpty(),
                    fontSize = 14.sp,
                    fontWeight = FontWeight.Bold,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.padding(top = 20.dp, start = 14.dp, end = 60.dp),
                )

                ProductRating(product, modifier = Modifier.padding(start = 14.dp))

                Spacer(Modifier.weight(1f))
                Prices(product, modifier = Modifier.padding(start = 14.dp))
            }
        }

        FavoriteButton(
            isFavorite = product.isFavorite,
            tint = if (product.isFavorite) FavoriteYellow else GrayRampDarkestLight,
            modifier = Modifier
                .align(Alignment.TopEnd)
                .padding(top = 25.dp, end = 34.dp),
            onClick = { onFavoriteClick(product) },
        )

        ProductCircleCartButton(
            isInCart = product.cartCount > 0,
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .padding(end = 24.dp),
            onClick = { onCartClick(product) }
        )

        ProductLabelsList(
            product.labels.orEmpty(),
            modifier = Modifier
                .align(Alignment.TopStart)
                .padding(top = 8.dp, start = 2.dp)
        )
    }
}

@Preview
@Composable
fun ProductHorizontalPreview() {
    ProductHorizontal(mockPopularProducts.first(), {}, {}, {})
}