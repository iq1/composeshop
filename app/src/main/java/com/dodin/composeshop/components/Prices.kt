package com.dodin.composeshop.components

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.dodin.composeshop.presentation.main.home.mockNewProducts
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.dodin.composeshop.theme.Gray950
import com.dodin.composeshop.theme.GrayRamp
import com.dodin.composeshop.tools.toPrettyPrice

@Composable
fun Prices(product: ProductModel, modifier: Modifier = Modifier) {
    Column(
        modifier = modifier
    ) {
        val price = product.specialPrice ?: product.price
        val oldPrice = product.price
        if (product.specialPrice != null) {
            Text(
                text = oldPrice?.toPrettyPrice(product.currency).orEmpty(),
                fontSize = 13.sp,
                color = GrayRamp,
                textDecoration = TextDecoration.LineThrough,
            )
        }
        Text(
            text = price?.toPrettyPrice(product.currency).orEmpty(),
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = Gray950,
        )
    }
}

@Preview
@Composable
fun PricesPreview() {
    Prices(mockNewProducts.first())
}