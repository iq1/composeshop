package com.dodin.composeshop.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.dodin.composeshop.theme.FavoriteYellow
import com.dodin.composeshop.theme.GrayRampDarkestLight

@Composable
fun ProductVertical(
    product: ProductModel,
    onProductClick: (ProductModel) -> Unit,
    onFavoriteClick: (ProductModel) -> Unit,
    onCartClick: (ProductModel) -> Unit,
) {
    Box(modifier = Modifier
        .width(180.dp)
        .fillMaxHeight()
        .clickable { onProductClick(product) }
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth(),
        ) {
            Image(
                painter = rememberImagePainter(product.image),
                contentDescription = product.name,
                modifier = Modifier.size(135.dp, 135.dp),
            )
            Text(
                text = product.name.orEmpty(),
                fontSize = 14.sp,
                fontWeight = FontWeight.Bold,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.padding(top = 20.dp, start = 16.dp, end = 16.dp),
            )
        }

        FavoriteButton(
            isFavorite = product.isFavorite,
            tint = if (product.isFavorite) FavoriteYellow else GrayRampDarkestLight,
            modifier = Modifier
                .align(Alignment.TopEnd)
                .padding(top = 8.dp),
            onClick = { onFavoriteClick(product) },
        )

        Prices(
            product, modifier = Modifier
                .align(Alignment.BottomStart)
                .padding(start = 18.dp)
        )

        ProductCircleCartButton(
            isInCart = product.cartCount > 0,
            modifier = Modifier.align(Alignment.BottomEnd),
            onClick = { onCartClick(product) }
        )

        ProductLabelsList(
            product.labels.orEmpty(),
            modifier = Modifier
                .align(Alignment.TopStart)
                .padding(top = 8.dp, start = 2.dp)
        )
    }
}