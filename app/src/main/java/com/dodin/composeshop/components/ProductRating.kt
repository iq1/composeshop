package com.dodin.composeshop.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dodin.composeshop.R
import com.dodin.composeshop.presentation.main.home.mockNewProducts
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.dodin.composeshop.theme.FavoriteYellow
import com.dodin.composeshop.theme.GrayRamp
import com.dodin.composeshop.theme.GrayRampDarkestLight

@Composable
fun ProductRating(product: ProductModel, modifier: Modifier = Modifier) {
    Row(verticalAlignment = Alignment.CenterVertically, modifier = modifier) {
        Stars(if (product.ratingCount == 0) 1 else 5)
        Text(
            text = if (product.ratingCount == 0) stringResource(R.string.no_reviews)
            else product.ratingCount.toString(),
            color = GrayRamp,
            fontSize = 11.sp,
            modifier = Modifier.padding(start = 6.dp)
        )
    }
}

@Composable
fun Stars(count: Int = 1) {
    repeat(count) {
        Icon(
            imageVector = Icons.Filled.Star,
            modifier = Modifier.size(20.dp),
            tint = if (count == 1) GrayRampDarkestLight else FavoriteYellow,
            contentDescription = null
        )
    }
}

@Preview
@Composable
fun ProductRatingPreview() {
    ProductRating(mockNewProducts.first())
}

@Preview
@Composable
fun ProductNoRatingPreview() {
    ProductRating(mockNewProducts.last())
}