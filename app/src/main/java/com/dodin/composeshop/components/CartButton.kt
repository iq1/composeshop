package com.dodin.composeshop.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dodin.composeshop.R
import com.dodin.composeshop.presentation.main.home.products
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.dodin.composeshop.theme.FavoriteYellow
import com.dodin.composeshop.tools.toPrettyPrice
import java.math.BigDecimal

@Composable
fun CartButton(product: ProductModel, modifier: Modifier = Modifier) {
    if (product.isAvailable) {
        CartActiveButton(product, modifier)
    } else {
        ShopAppPrimaryButton(
            text = stringResource(R.string.not_available_string),
            enabled = product.isAvailable,
            modifier = modifier
        ) {

        }
    }
}

@Composable
private fun CartActiveButton(product: ProductModel, modifier: Modifier = Modifier) {
    Row(
        modifier = modifier
            .height(60.dp)
            .background(Color.Black, MaterialTheme.shapes.medium)
            .clickable { },
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Icon(
            painter = painterResource(R.drawable.custom_shop_product_button_cart),
            tint = FavoriteYellow,
            modifier = Modifier
                .padding(start = 18.dp),
            contentDescription = null
        )

        Column(modifier = Modifier.padding(start = 18.dp)) {
            Text(
                text = stringResource(R.string.available_string),
                fontSize = 12.sp,
                color = Color.White,
                modifier = Modifier.alpha(0.4f)
            )
            Text(
                text = stringResource(R.string.into_the_basket_string),
                fontSize = 17.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White
            )
        }

        Spacer(Modifier.weight(1f))
        PricesColumn(product)
    }
}

@Composable
private fun PricesColumn(product: ProductModel) {
    Column(modifier = Modifier.padding(end = 18.dp)) {
        val oldPrice = if (product.specialPrice != null) product.price else null
        val price = product.specialPrice ?: product.price

        oldPrice?.let {
            Text(
                text = it.toPrettyPrice(product.currency),
                textDecoration = TextDecoration.LineThrough,
                color = Color.White,
                modifier = Modifier
                    .padding(start = 18.dp)
                    .alpha(0.4f),
            )
        }

        Text(
            text = price?.toPrettyPrice(product.currency).orEmpty(),
            color = Color.White,
            modifier = Modifier
                .padding(start = 18.dp),
        )
    }
}

@Preview
@Composable
fun CartNotAvailableButtonPreview() {
    CartButton(products.first())
}

@Preview
@Composable
fun CartAvailableButtonPreview() {
    CartButton(products.last())
}

@Preview
@Composable
fun CartAvailableButtonWithOldPricePreview() {
    CartButton(products.last().copy(specialPrice = BigDecimal(10)))
}