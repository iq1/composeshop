package com.dodin.composeshop.tools

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols


object ProductTool {
    val prettyNumberFormat = DecimalFormat("#0.00")

    init {
        val formatSymbols = DecimalFormatSymbols()
        formatSymbols.decimalSeparator = '.'
        prettyNumberFormat.decimalFormatSymbols = formatSymbols
    }
}

fun Number.toPrettyPrice(currency: String? = null, currencyDivider: String = " "): String {
    var formatted = ProductTool.prettyNumberFormat.format(this)
    if (formatted.endsWith("00")) {
        formatted = formatted.dropLast(3)
    }
    if (currency != null && currency.isNotEmpty()) {
        formatted += "$currencyDivider$currency"
    }
    return formatted
}