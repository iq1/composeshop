package com.dodin.composeshop.tools

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation

/**
 * TODO: Need to be improved
 * especially **transformedToOriginal** method
 *
 * use constants and constructor parameters instead of hardcode
 */
class MaskTransformation : VisualTransformation {
    // Max length = 10 (0660037681)
    // Max transformed length 18 (+38(066) 003 76 81)
    override fun filter(text: AnnotatedString): TransformedText {
        val prefix = "+38"
        val mask = "(###) ### ## ##"

        val maskedText = prefix + mask
        val trimmed = if (text.text.length >= 10) text.text.substring(0..9) else text.text
        var out = maskedText.takeWhile { it != '#' }

        for (i in trimmed.indices) {
            out += trimmed[i]
            if (i == 2) out += ") "
            if (i == 5) out += " "
            if (i == 7) out += " "
        }

        val phoneMaskMapping = object : OffsetMapping {
            override fun originalToTransformed(offset: Int): Int {
                if (offset <= 2) return offset + 4
                if (offset <= 6) return offset + 6
                if (offset <= 7) return offset + 7
                if (offset <= 9) return offset + 8

                return maskedText.length
            }

            override fun transformedToOriginal(offset: Int): Int {
                val transformed = when {
                    offset <= 7 -> offset
                    offset <= 15 -> offset - 8
                    else -> offset - 8 // 8 is difference between `max transformed length` and `max raw length` = 10
                }
                return transformed // raw input length(0660037681)
            }
        }

        return TransformedText(AnnotatedString(out), phoneMaskMapping)
    }
}
