package com.dodin.composeshop.tools

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import org.koin.androidx.viewmodel.ViewModelOwner
import org.koin.androidx.viewmodel.scope.getViewModel
import org.koin.core.annotation.KoinInternalApi
import org.koin.core.context.GlobalContext
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier
import org.koin.core.scope.Scope

@OptIn(KoinInternalApi::class)
@Composable
inline fun <reified T : ViewModel> getViewModel(
    navController: NavController,
    route: String? = null,
    qualifier: Qualifier? = null,
    scope: Scope = GlobalContext.get().scopeRegistry.rootScope,
    noinline parameters: ParametersDefinition? = null,
): T {
    val owner = if (route != null)
        try {
            navController.getBackStackEntry(route)
        } catch (e: Exception) {
            navController.currentBackStackEntry!!
        }
    else
        navController.currentBackStackEntry!!

    return remember(qualifier, parameters) {
        scope.getViewModel(qualifier, { ViewModelOwner.from(owner) }, parameters)
    }
}