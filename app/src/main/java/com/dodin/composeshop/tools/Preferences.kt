package com.dodin.composeshop.tools

import android.content.SharedPreferences
import androidx.core.content.edit
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class StringPreferenceDelegate<T>(
    private val sharedPreferences: SharedPreferences,
    private val defaultValue: String = "",
    private val key: String? = null
) : ReadWriteProperty<T, String> {
    override fun getValue(thisRef: T, property: KProperty<*>): String {
        return sharedPreferences.getString(key ?: property.name, defaultValue)!!
    }

    override fun setValue(thisRef: T, property: KProperty<*>, value: String) {
        sharedPreferences.edit { putString(key ?: property.name, value) }
    }
}

fun <T> SharedPreferences.stringPref(defaultValue: String = "", key: String? = null) =
    StringPreferenceDelegate<T>(this, defaultValue, key)