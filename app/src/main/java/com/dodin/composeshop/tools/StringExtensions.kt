package com.dodin.composeshop.tools

fun String.formatWithMask(mask: String, maskSymbol: Char = '#'): String {
    var resultPhone = ""
    var i = 0
    this.forEach {
        if (i <= mask.lastIndex)
            if (mask[i] != maskSymbol) {
                resultPhone += mask.substring(i, mask.lastIndex).takeWhile { it != maskSymbol }
                    .apply { i += this.length }
            }
        resultPhone += it
        i++
    }
    return resultPhone
}

fun String.replace(vararg arguments: Pair<String, String>): String =
    arguments.fold(this, { acc, (arg, param) -> acc.replace(arg, param) })