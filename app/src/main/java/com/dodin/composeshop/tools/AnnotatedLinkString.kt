package com.dodin.composeshop.tools

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString

class AnnotatedLinkString(
    private val text: String,
    private val linkifyText: String,
    private val url: String,
    private val linkColor: Color = Color.Blue,
) {
    val value = buildAnnotatedString {
        val startIndex = text.indexOf(linkifyText)
        val endIndex = startIndex + linkifyText.length

        append(text)
        addStyle(
            style = SpanStyle(color = linkColor),
            start = startIndex,
            end = endIndex
        )
        addStringAnnotation(URL_TAG, url, startIndex, endIndex)
    }

    fun getAnnotations(start: Int, end: Int) =
        value.getStringAnnotations(URL_TAG, start, end)

    private companion object {
        const val URL_TAG = "url"
    }
}