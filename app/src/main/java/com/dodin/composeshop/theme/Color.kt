package com.dodin.composeshop.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val GrayRamp = Color(0xFF8E8E93)
val GrayRampLight = Color(0xFFE5E5EA)
val Gray292 = Color(0xFF292929)
val Gray900 = Color(0xFF212121)
val Gray950 = Color(0xFF141414)
val GrayRampDarkestLight = Color(0xFFD1D1D6)
val GrayRampLightenest = Color(0xFFEFEFF4)
val Black30alpha = Color(0x33000000)

val ColorPrimaryDisable = Color(0x66141414)
val FavoriteYellow = Color(0xFFFFE01A)
val PressedYellow = Color(0xFFF5E36C)