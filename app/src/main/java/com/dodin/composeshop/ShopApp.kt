package com.dodin.composeshop

import android.app.Application
import com.dodin.composeshop.di.applicationModule
import com.dodin.composeshop.di.netWorkModule
import com.dodin.composeshop.di.repositoryModule
import com.dodin.composeshop.di.viewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class ShopApp : Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())

        startKoin {
            androidContext(this@ShopApp)
            modules(viewModelsModule, repositoryModule, netWorkModule, applicationModule)
        }
    }
}