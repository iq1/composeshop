package com.dodin.composeshop

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import com.dodin.composeshop.core.base.NavigationManager
import com.dodin.composeshop.data.storage.AppPreferences
import com.dodin.composeshop.presentation.main.ShowMainScreenRoute
import com.dodin.composeshop.presentation.onboarding.ShowOnBoarding
import com.dodin.composeshop.theme.ComposeShopTheme
import org.koin.android.ext.android.inject

class MainActivity : ComponentActivity() {
    private val navigationManager: NavigationManager by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)

        // TODO: temporary solution. Move to viewModel
        val appPreferences: AppPreferences by inject()
        setContent {
            ComposeShopTheme {
                Surface(color = MaterialTheme.colors.background) {
                    NavGraph(
                        if (appPreferences.token.isNotEmpty()) ShowMainScreenRoute.route
                        else ShowOnBoarding.route,
                        navigationManager
                    )
                }
            }
        }
    }
}