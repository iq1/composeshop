package com.dodin.composeshop.data.network.models.products

import com.dodin.composeshop.data.network.models.screens.CartNetworkModel
import com.dodin.composeshop.data.network.models.screens.CustomerNetworkModel
import com.google.gson.annotations.SerializedName

data class CatalogProductResponseModel(
    @SerializedName("catalogProduct")
    val catalogProduct: HomeProductResponseModel,
    @SerializedName("customerCart")
    val cart: CartNetworkModel? = null,
    @SerializedName("cart")
    val anonymousCart: CartNetworkModel? = null,
    @SerializedName("customer")
    val customer: CustomerNetworkModel? = null,
)