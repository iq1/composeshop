package com.dodin.composeshop.data.network.repository

import com.dodin.composeshop.core.base.repository.NetworkRepository
import com.dodin.composeshop.data.network.ApiWorker
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.google.gson.Gson
import ua.brander.core.functional.map
import ua.brander.core.platform.NetworkHandler

class ProductRepository(
    private val apiWorker: ApiWorker,
    private val networkHandler: NetworkHandler,
    private val gson: Gson,
) : NetworkRepository {

    override fun isConnected() = networkHandler.isConnected

    suspend fun getFullProduct(id: Int) =
        networkRequest { apiWorker.getFullProduct(id).execute(gson) }.map {
            val cart = it.cart ?: it.anonymousCart
            ProductModel.convertFromNetworkResponse(
                it.catalogProduct,
                it.customer?.wishList?.items.orEmpty(),
                it.customer?.wishList?.items.orEmpty().mapNotNull { it.product.sku },
                cart,
            )
        }
}