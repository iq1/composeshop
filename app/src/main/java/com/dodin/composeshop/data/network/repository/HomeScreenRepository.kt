package com.dodin.composeshop.data.network.repository

import com.dodin.composeshop.core.base.repository.NetworkRepository
import com.dodin.composeshop.data.network.ApiWorker
import com.dodin.composeshop.presentation.models.screens.HomeScreenModel
import com.google.gson.Gson
import ua.brander.core.functional.map
import ua.brander.core.platform.NetworkHandler

class HomeScreenRepository(
    private val apiWorker: ApiWorker,
    private val networkHandler: NetworkHandler,
    private val gson: Gson,
) : NetworkRepository {

    override fun isConnected() = networkHandler.isConnected

    suspend fun getData() = networkRequest { apiWorker.getHomeScreenData().execute(gson) }.map {
        HomeScreenModel.convertFromNetworkResponse(it)
    }
}