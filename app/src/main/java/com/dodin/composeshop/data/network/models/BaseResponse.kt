package com.dodin.composeshop.data.network.models

data class BaseResponse<T>(val data : T?, val errors : List<Map<String, Any>>? = null)