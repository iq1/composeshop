package com.dodin.composeshop.data.network.models.products

import com.google.gson.annotations.SerializedName

data class HitProductsResponseModel(
    @SerializedName("mainScreenHitProducts")
    val products: List<HomeProductResponseModel>,
)

data class NewProductsResponseModel(
    @SerializedName("mainScreenNewProducts")
    val products: List<HomeProductResponseModel>,
)

enum class StockStatus {
    IN_STOCK, OUT_OF_STOCK
}

data class HomeProductResponseModel(
    @SerializedName("id")
    val id: Int,
    @SerializedName("sku")
    val sku: String?,
    @SerializedName("stock_status")
    val stockStatus: StockStatus,
    @SerializedName("small_image")
    val image: ImageResponseModel?,
    @SerializedName("name")
    val name: String,
    @SerializedName("special_price")
    val specialPrice: String?,
    @SerializedName("price_range")
    val priceRange: PriceRangeResponseModel?,
    @SerializedName("rating_summary")
    val ratingSummary: String?,
    @SerializedName("reviews_count")
    val reviewsCount: String?,
    @SerializedName("product_labels")
    val labels: List<LabelResponseModel>?,
    @SerializedName("in_wish_list")
    val isFavorite: Boolean = false,
    @SerializedName("short_description")
    val shortDescription: DescriptionResponseModel?,
    @SerializedName("mobile_description")
    val description: String?,
    @SerializedName("media_gallery")
    val mediaGallery: List<MediaGalleryResponseModel>?,
    @SerializedName("reviews")
    val reviews: List<ReviewsResponseModel>?,
    @SerializedName("attributes_on_product_list")
    val attributes: List<AttributeResponseModel>?,
    @SerializedName("url_rewrites")
    val urls: List<UrlRewritesResponseModel>?,
    @SerializedName("cross_sells")
    val crosssellProducts: List<HomeProductResponseModel>?,
    @SerializedName("configurable_options")
    val configurableOptions: List<ConfigurableOptionResponseModel>?,
    @SerializedName("variants")
    val variants: List<VariantResponseModel>?
)

data class ImageResponseModel(
    @SerializedName("url")
    val url: String,
)

data class PriceRangeResponseModel(
    @SerializedName("minimum_price")
    val minimumPrice: PriceResponseModel,
    @SerializedName("maximum_price")
    val maximumPrice: PriceResponseModel,
)

data class PriceResponseModel(
    @SerializedName("regular_price")
    val regularPrice: MoneyResponseModel,
)

data class MoneyResponseModel(
    @SerializedName("currency")
    val currency: String,
    @SerializedName("value")
    val value: String,
)

data class LabelResponseModel(
    @SerializedName("title")
    val title: String,
    @SerializedName("product_custom_style")
    val customStyle: String,
)

data class DescriptionResponseModel(
    @SerializedName("html")
    val html: String,
)

data class MediaGalleryResponseModel(
    @SerializedName("url")
    val url: String,
    @SerializedName("disabled")
    val isDisabled: Boolean,
)

data class ReviewsResponseModel(
    @SerializedName("nickname")
    val nickname: String,
    @SerializedName("detail")
    val text: String,
    @SerializedName("rating_votes")
    val rating: List<RatingNetworkModel>,
    @SerializedName("created_at")
    val date: String,
)

data class RatingNetworkModel(
    @SerializedName("value")
    val value: Int,
)

data class AttributeResponseModel(
    @SerializedName("attribute_label")
    val label: String,
    @SerializedName("attribute_value")
    val value: String,
)

data class UrlRewritesResponseModel(
    @SerializedName("url")
    val url: String,
)

data class ConfigurableOptionResponseModel(
    @SerializedName("id")
    val id: Int,
    @SerializedName("attribute_id")
    val attributeId: String,
    @SerializedName("label")
    val label: String,
    @SerializedName("position")
    val position: Int,
    @SerializedName("use_default")
    val useDefault: Boolean,
    @SerializedName("attribute_code")
    val attributeCode: String,
    @SerializedName("product_id")
    val productId: Int,
    @SerializedName("values")
    val values: List<ConfigurableOptionsValueResponseModel>?
)

data class ConfigurableOptionsValueResponseModel(
    @SerializedName("value_index")
    val valueIndex: Int,
    @SerializedName("label")
    val label: String,
    @SerializedName("swatch_data")
    val swatchData: SwatchDataResponseModel?
)

data class SwatchDataResponseModel(
    @SerializedName("value")
    val value: String
)

data class VariantResponseModel(
    @SerializedName("attributes")
    val attributes: List<VariantAttributeResponseModel>?,
    @SerializedName("product")
    val product: HomeProductResponseModel?
)

data class VariantAttributeResponseModel(
    @SerializedName("code")
    val code: String,
    @SerializedName("label")
    val label: String,
    @SerializedName("value_index")
    val valueIndex: Int
)