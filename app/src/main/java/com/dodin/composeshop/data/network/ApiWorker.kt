package com.dodin.composeshop.data.network

import com.dodin.composeshop.core.base.CallWrapper
import com.dodin.composeshop.data.network.graph_ql.GraphQlRequestBuilder
import com.dodin.composeshop.data.network.graph_ql.GraphQlRequests
import com.dodin.composeshop.data.network.models.BaseResponse
import com.dodin.composeshop.data.network.models.products.CatalogProductResponseModel
import com.dodin.composeshop.data.network.models.registration.*
import com.dodin.composeshop.data.network.models.screens.HomeScreenResponseModel
import com.dodin.composeshop.data.storage.AppPreferences
import com.google.gson.Gson
import okhttp3.OkHttpClient

class ApiWorker(
    private val appPreferences: AppPreferences,
    okHttpClient: OkHttpClient,
    gson: Gson,
) : BaseGraphQlApiWorker(okHttpClient, gson) {

    private fun isAuth() = appPreferences.token.isNotEmpty()
    private fun cartId() =
        if (isAuth()) appPreferences.customerCartId else appPreferences.anonymousCartId

    override fun getHeaders(): Map<String, String> = if (appPreferences.token.isNotEmpty()) {
        mapOf(
            "Authorization" to "Bearer ${appPreferences.token}",
            "Authorization-Secret" to appPreferences.secret
        )
    } else emptyMap()

    fun sendPhone(
        sendPhoneRequestModel: SendPhoneRequestModel
    ): CallWrapper<BaseResponse<SendPhoneResponse>> = makeCall(
        GraphQlRequestBuilder.createBuilder(
            GraphQlRequests.buildPhoneQuery(sendPhoneRequestModel.phone)
        ).build()
    )

    fun resendPhone(
        resendSmsRequestModel: ResendSmsRequestModel
    ): CallWrapper<BaseResponse<ResendSmsResponse>> = makeCall(
        GraphQlRequestBuilder.createBuilder(
            GraphQlRequests.buildResendSmsQuery(resendSmsRequestModel.hash)
        ).build()
    )

    fun validateOtp(validateSmsCodeRequest: ValidateSmsCodeRequestModel):
            CallWrapper<BaseResponse<ValidateSmsCodeResponse>> = makeCall(
        GraphQlRequestBuilder.createBuilder(
            GraphQlRequests.buildValidateSmsCodeRequest(
                validateSmsCodeRequest.hash,
                validateSmsCodeRequest.isNew,
                validateSmsCodeRequest.otpCode,
                validateSmsCodeRequest.phone
            )
        ).build()
    )

    fun getHomeScreenData(): CallWrapper<BaseResponse<HomeScreenResponseModel>> = makeCall(
        GraphQlRequestBuilder.createBuilder(
            GraphQlRequests.generateHomeQuery(isAuth(), cartId())
        ).build()
    )

    fun getFullProduct(id: Int): CallWrapper<BaseResponse<CatalogProductResponseModel>> = makeCall(
        GraphQlRequestBuilder.createBuilder(
            GraphQlRequests.generateFullProductQuery(isAuth(), id, cartId())
        ).build()
    )
}