package com.dodin.composeshop.data.network.models.registration

import com.google.gson.annotations.SerializedName

data class SendPhoneRequestModel(val phone:String)

data class SendPhoneResponse(
    @SerializedName("sendPhone")
    val sendPhone: SendPhoneResponseModel
)

data class SendPhoneResponseModel(
    @SerializedName("is_new")
    val isNew: Boolean,
    @SerializedName("hash")
    val hash: String
)