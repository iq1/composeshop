package com.dodin.composeshop.data.network.models.registration

import com.google.gson.annotations.SerializedName

data class ResendSmsRequestModel(val hash: String)

data class ResendSmsResponseModel(
    val message: String,
    val success: Boolean,
    val canShowResend: Boolean
)

data class ResendSmsResponse(
    @SerializedName("resendSms")
    val resendSmsResponseModel: ResendSmsResponseModel
)