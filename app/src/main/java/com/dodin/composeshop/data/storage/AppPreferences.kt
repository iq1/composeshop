package com.dodin.composeshop.data.storage

import android.content.SharedPreferences
import com.dodin.composeshop.tools.stringPref

class AppPreferences(private val sharedPreferences: SharedPreferences) {
    var token: String by sharedPreferences.stringPref()
    var secret: String by sharedPreferences.stringPref()
    var customerCartId: String by sharedPreferences.stringPref()
    var anonymousCartId: String by sharedPreferences.stringPref()
}