package com.dodin.composeshop.data.network.graph_ql

object GraphQlRequests {

    const val QUERY_KEY = "query"

    fun buildPhoneQuery(phone: String): String = """query{
          |sendPhone(phone: "$phone") {
          |  is_new
          |  hash
          |}
          |}""".trimMargin()

    fun buildResendSmsQuery(phone: String) = """query{
          |resendSms($phone) {
          |    message
          |    success
          |    canShowResend
          |}
          |}""".trimMargin()

    fun buildValidateSmsCodeRequest(
        hash: String,
        isNew: Boolean,
        otpCode: String,
        phone: String,
    ) = """query{
          |validateSmsCode(
          |    hash:"$hash"
          |    is_new:$isNew
          |    otp_code:"$otpCode"
          |    phone:"$phone"
          |) {
          |    success
          |    customer_token{
          |      token
          |      secret
          |    }
          |}
          |}""".trimMargin()

    enum class HomeProductType(val query: String) {
        NEW(
            """
            mainScreenNewProducts(
                input: {
                  from: "news_from_date"
                  to: "news_to_date"
                  item: "home_product_news_item"
                  namespace: "home_product_news"
                  position: "home_product_news_position"
                }
              )
        """.trimIndent()
        ),
        HIT(
            """
            mainScreenHitProducts(
                input: {
                  from: "hit_from_date"
                  to: "hit_to_date"
                  item: "home_product_hits_item"
                  namespace: "home_product_hits"
                  position: "home_product_hits_position"
                }
              )
        """.trimIndent()
        ),
    }

    fun generateHomeProductsQuery(type: HomeProductType): String {
        return type.query + FULL_PRODUCT.trimIndent()
    }

    private const val FULL_PRODUCT = """
        {
            id
            sku
            stock_status
            in_wish_list
            product_labels {
              title
              product_custom_style
            }
            rating_summary
            reviews_count
            image {
              url
            }
            small_image {
              url
            }
            thumbnail {
              url
            }
            name
            new_from_date
            new_to_date
            special_price
            price_range {
              minimum_price {
                regular_price {
                  currency
                  value
                }
              }
              maximum_price {
                regular_price {
                  currency
                  value
                }
              }
            }
        }
        """

    private const val SUBQUERY_CUSTOMER_CART = """
        |customerCart {
        |        items {
        |          id
        |          product {
        |            id
        |            sku
        |          }
        |          quantity
        |        }
        |      }
    """

    private fun generateCartSubQuery(cartId: String) = if (cartId.isNotEmpty()) {
        """
        cart(cart_id: "$cartId"){
            items {
              id
              product {
                id
                sku
              }
              quantity
            }
          }
    """.trimIndent()
    } else {
        ""
    }

    private const val SUBQUERY_CUSTOMER_WISHLIST = """
        |customer {
        |    wishlist {
        |      items {
        |        id
        |        product {
        |          id
        |          sku
        |        }
        |      }
        |    }
        |  }
    """

    const val SUBQUERY_BANNERS = """
        |mainScreenBanners {
        |    banners {
        |      mobile_banner
        |    }
        |  }
    """

    private const val SUBQUERY_HOME_CATEGORIES = """
        |mainScreenCategories{
        |    id
        |    image
        |    mobile_image
        |    name
        |    position
        |    level
        |    url_key
        |  }
    """

    private const val POPULAR_POSTS = """blogPostList(
		filters:{is_popular:{eq:"1"}}
	) {
		entity_id,
		title,
		url_key,
		full_url,
		description
		category_ids
		content_nav_menu
		author
		content
		preview_image,
		image,
		created_at,
		updated_at,
	}"""

    fun generateHomeQuery(isAuth: Boolean, cartId: String = ""): String {
        val cart = if (isAuth) SUBQUERY_CUSTOMER_CART.trimMargin() else generateCartSubQuery(cartId)
        val wishList = if (isAuth) SUBQUERY_CUSTOMER_WISHLIST.trimMargin() else ""
        return "query {" +
                cart +
                wishList +
                SUBQUERY_BANNERS.trimMargin() +
                SUBQUERY_HOME_CATEGORIES.trimMargin() +
                generateHomeProductsQuery(HomeProductType.NEW) +
                generateHomeProductsQuery(HomeProductType.HIT) +
                POPULAR_POSTS.trimIndent() +
                "}"
    }

    fun generateFullProductQuery(isAuth: Boolean, id: Int, cartId: String = ""): String {
        val cart = if (isAuth) SUBQUERY_CUSTOMER_CART.trimMargin() else generateCartSubQuery(cartId)
        val wishList = if (isAuth) SUBQUERY_CUSTOMER_WISHLIST.trimMargin() else ""

        val crossSellProducts = "crosssell_products $FULL_PRODUCT"
        return """
            query {
              $cart
              $wishList
              catalogProduct(entityId: %d) {
                id
                sku
                articul
                name
                $crossSellProducts
                stock_status
                mobile_description
                short_description {
                  html
                }
                media_gallery {
                  url
                }
                resized {image{url}}
                reviews {
                  nickname
                  created_at
                  detail
                  rating_votes {
                    value
                  }
                }
                attributes_on_product_list(exclude_attributes: ["short_description"]) {
                  attribute_label
                  attribute_value
                }
                url_rewrites {
                  url
                }
                product_labels {
                  title
                  product_custom_style
                }
                rating_summary
                reviews_count
                image {
                  url
                }
                special_price
                price_range {
                  minimum_price {
                    regular_price {
                      currency
                      value
                    }
                  }
                  maximum_price {
                    regular_price {
                      currency
                      value
                    }
                  }
                }
                $CONFIGURABLE_PRODUCT
              }
            }
        """.trimIndent().format(id)
    }

    private const val PRODUCT_PARAMETERS = """
                    id
                    sku
                    articul
                    stock_status
                    in_wish_list
                    product_labels {
                      title
                      product_custom_style
                    }
                    rating_summary
                    reviews_count
                    image {
                      url
                    }
                    small_image {
                      url
                    }
                    thumbnail {
                      url
                    }
                    media_gallery {
                        url
                    }
                    resized {image{url}}
                    name
                    new_from_date
                    new_to_date
                    special_price
                    price_range {
                      minimum_price {
                        regular_price {
                          currency
                          value
                        }
                      }
                      maximum_price {
                        regular_price {
                          currency
                          value
                        }
                      }
                    }
    """

    private const val CONFIGURABLE_PRODUCT = """
        ... on ConfigurableProduct {
        variants 	{
          attributes {
            label
            code
            value_index
          }
          product{$PRODUCT_PARAMETERS}
        }
         configurable_options {
          id
          attribute_id
          label
          position
          use_default
          attribute_code
          values {
            value_index
            label
             swatch_data{
             	value
            }
          }
          product_id
        }
      }
    """
}