package com.dodin.composeshop.data.network.models.banners

import com.google.gson.annotations.SerializedName

data class BannersResponseModel(
    @SerializedName("mainScreenBanners")
    val mainScreenBanners: MainScreenBannersNetworkModel
)

data class MainScreenBannersNetworkModel(
    @SerializedName("banners")
    val banners: List<BannerNetworkModel>
)

data class BannerNetworkModel(
    @SerializedName("mobile_banner")
    val url: String,
)