package com.dodin.composeshop.data.network.models.registration

import com.google.gson.annotations.SerializedName

data class ValidateSmsCodeRequestModel(
    @SerializedName("phone")
    val phone: String,
    @SerializedName("is_new")
    val isNew: Boolean,
    @SerializedName("hash")
    val hash: String,
    @SerializedName("otp_code")
    val otpCode: String
)

data class ValidateSmsCodeResponseModel(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("customer_token")
    val customerToken: ValidateSmsTokenResponseModel
)

data class ValidateSmsTokenResponseModel(val token: String, val secret: String)

data class ValidateSmsCodeResponse(
    @SerializedName("validateSmsCode")
    val validateSmsCodeResponseModel: ValidateSmsCodeResponseModel
)