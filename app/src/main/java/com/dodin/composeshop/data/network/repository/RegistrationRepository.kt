package com.dodin.composeshop.data.network.repository

import com.dodin.composeshop.core.base.repository.NetworkRepository
import com.dodin.composeshop.data.network.ApiWorker
import com.dodin.composeshop.data.network.models.registration.ResendSmsRequestModel
import com.dodin.composeshop.data.network.models.registration.SendPhoneRequestModel
import com.dodin.composeshop.data.network.models.registration.ValidateSmsCodeRequestModel
import com.dodin.composeshop.presentation.models.IsNewUserWithHashModel
import com.dodin.composeshop.presentation.models.ResendCodeModel
import com.dodin.composeshop.presentation.models.ValidationCodeModel
import com.google.gson.Gson
import ua.brander.core.functional.map
import ua.brander.core.platform.NetworkHandler

class RegistrationRepository(
    private val networkHandler: NetworkHandler,
    private val apiWorker: ApiWorker,
    private val gson: Gson,
) : NetworkRepository {

    override fun isConnected() = networkHandler.isConnected

    suspend fun sendPhone(sendPhoneRequestModel: SendPhoneRequestModel) = networkRequest {
        apiWorker.sendPhone(sendPhoneRequestModel).execute(gson)
    }.map {
        IsNewUserWithHashModel.convertFromNetworkResponse(it.sendPhone)
    }

    suspend fun resendCode(resendSmsRequestModel: ResendSmsRequestModel) = networkRequest {
        apiWorker.resendPhone(resendSmsRequestModel).execute(gson)
    }.map {
        ResendCodeModel.convertFromNetworkResponse(it)
    }

    suspend fun validateOtp(validateSmsCodeRequest: ValidateSmsCodeRequestModel) = networkRequest {
        apiWorker.validateOtp(validateSmsCodeRequest).execute(gson)
    }.map {
        // TODO: add db
//        if (it.data.validateSmsCodeResponseModel.success) {
//            val userDao = appDatabase.getUserProfileDataDao()
//            userDao.addBaseUserData(
//                UserProfileDataEntity(
//                    userPhone = validateSmsCodeRequest.phone.replace(
//                        "(",
//                        ""
//                    ).replace(")", "")
//                )
//            )
//        }
        ValidationCodeModel.convertFromNetworkResponse(it)
    }
}