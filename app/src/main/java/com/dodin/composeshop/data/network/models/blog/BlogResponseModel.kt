package com.dodin.composeshop.data.network.models.blog


import com.google.gson.annotations.SerializedName

data class BlogResponseModel(
    @SerializedName("entity_id")
    val entityId: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("url_key")
    val urlKey: String?,
    @SerializedName("full_url")
    val fullUrl: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("category_ids")
    val categoryIds: List<Int>?,
    @SerializedName("content_nav_menu")
    val contentNavMenu: String?,
    @SerializedName("author")
    val author: String?,
    @SerializedName("content")
    val content: String?,
    @SerializedName("preview_image")
    val previewImage: String?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("updated_at")
    val updatedAt: String?
)