package com.dodin.composeshop.data.network

import com.dodin.composeshop.ShopConfig
import com.dodin.composeshop.core.base.CallWrapper
import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody

abstract class BaseGraphQlApiWorker(
    val okHttpClient: OkHttpClient,
    val gson: Gson,
) {

    abstract fun getHeaders(): Map<String, String>

    inline fun <reified T : Any> makeCall(query: Map<String, String>): CallWrapper<T> {
        val call = query.toGraphQlRequest()
        return CallWrapper(okHttpClient.newCall(call))
    }

    fun Map<String, String>.toGraphQlRequest(): Request {
        val reqString = gson.toJson(this, Map::class.java)
        return Request.Builder()
            .url(ShopConfig.graphQlUrl)
            .post(reqString.toRequestBody("application/json".toMediaType()))
            .apply {
                getHeaders().forEach { (key, value) ->
                    addHeader(key, value)
                }
            }
            .build()
    }
}