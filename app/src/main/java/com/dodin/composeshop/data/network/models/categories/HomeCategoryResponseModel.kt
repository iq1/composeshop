package com.dodin.composeshop.data.network.models.categories

import com.google.gson.annotations.SerializedName

data class HomeCategoriesResponseModel(
    @SerializedName("mainScreenCategories")
    val categories: List<HomeCategoryResponseModel>
)

data class HomeCategoryResponseModel(
    @SerializedName("id")
    val id: Int,
    @SerializedName("url_key")
    val urlKey: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("mobile_image")
    val mobileImage: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("position")
    val position: Int,
    @SerializedName("level")
    val level: Int,
)