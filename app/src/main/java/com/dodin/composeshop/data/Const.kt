package com.dodin.composeshop.data

class Const {

    companion object {
        const val BASE_ENDPOINT = "graphql/"
    }

    object ResponseCode {
        const val VALIDATION_EXCEPTION = 422
        const val SERVER_ERROR = 500
        const val OK = 200
    }

    object Delay {
        const val SEC_1 = 1000L
        const val SEC_0_750 = 750L
        const val SEC_0_500 = 500L
        const val SEC_0_250 = 250L
        const val SEC_0_150 = 150L
    }

    object DefaultConst {
        const val SEARCH_AREA_IN_METERS = 2000 * 1000
        const val DEFAULT_PAGINATION_PAGE_LIMIT = 20
        const val MAX_RATING_STARS = 5
        const val ATTRIBUTES_COUNT = 5
        const val REVIEWS_COUNT = 3
        const val MAX_COMMENT_LINES = 5
        const val BOOLEAN_FALSE = "0"
        const val BOOLEAN_TRUE = "1"
        const val SEARCH_COUNT = 2
    }

    object IntentConst {
        const val PHONE_NUMBER_KEY = "phoneNumberKey"
        const val PROMOTION_MODEL_KEY = "promotionModelKey"
        const val PROFILE_MODEL_KEY = "profileModelKey"
        const val MAIN_SCREEN_CODE_KEY = "main_screen_code"
        const val SHOP_LIST_KEY = "shopListKey"
        const val SHOP_KEY = "shopKey"
        const val BONUS_HISTORY_KEY = "bonus_history_key"
        const val SHOP_PHOTO_LIST_KEY = "shopPhotoListKey"
        const val WITH_DISTANCE_KEY = "withDistanceKey"
        const val ID_KEY = "id"
        const val CATEGORY_ID_KEY = "id"
        const val CATEGORY_NAME_KEY = "name"
        const val CATEGORY_TYPE_KEY = "type"
        const val CATEGORY_CAT_KEY = "categoryId"
        const val CATEGORY_SELECTED_AGGREGATIONS = "selectedAggregations"
    }

    object GenderConst {
        const val MALE = 1
        const val FEMALE = 2
    }

    object RequestCode {
        const val SMS_KEY = 101
        const val PRODUCT_SEARCH_KEY = 102
        const val PRODUCT_AGGREGATION_KEY = 103
        const val PRODUCT_FILTER_KEY = 104
    }

    object ImagesCache {
        const val BRANDER_MOBILE_LOGO = "branderMobileLogo"
        const val BRANDER_SPLASH_SCREEN = "branderSplashScreen"
    }

    object ListStyle {
        const val LIST = "list"
        const val GRID = "grid"
    }

    object FilterConst {
        const val ATTRIBUTE_CODE_PRICE = "price"
        const val FILTER_TYPE_PRICE = "price"
        const val FILTER_TYPE_SELECT = "select"
        const val FILTER_TYPE_BOOLEAN = "boolean"
        const val CATEGORY_ID = "category_id"
    }

    object OrderStatus {
        const val CANCELLED = "canceled"
        const val COMPLETE = "complete"
        const val PENDING = "pending"
        const val HOLDED = "holded"
        const val CLOSED = "closed"
        const val LIQPAY_PENDING_PAYMENT = "liqpay_pending_payment"
        const val PAID = "liqpay_paid"
        const val PROCESSING = "processing"
        const val PAYMENT_CANCELED = "liqpay_canceled_hold"
        const val PAYMENT_ERROR = "liqpay_error"
        const val PAYMENT_SUCCESS = "liqpay_success_hold"
        const val FRAUD = "fraud"
        const val PAYMENT_REVIEW = "payment_review"
        const val PENDING_PAYMENT = "pending_payment"
        const val WFP_ERROR = "wfp_error"
        const val WFP_HOLD = "wfp_hold"
        const val WFP_PAID = "wfp_paid"
        const val WFP_PENDING_PAYMENT = "wfp_pending_payment"
    }
}