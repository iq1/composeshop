package com.dodin.composeshop.data.network.models.screens

import com.google.gson.annotations.SerializedName
import com.dodin.composeshop.data.network.models.banners.MainScreenBannersNetworkModel
import com.dodin.composeshop.data.network.models.blog.BlogResponseModel
import com.dodin.composeshop.data.network.models.categories.HomeCategoryResponseModel
import com.dodin.composeshop.data.network.models.products.HomeProductResponseModel

data class HomeScreenResponseModel(
    @SerializedName("mainScreenBanners")
    val banners: MainScreenBannersNetworkModel,
    @SerializedName("mainScreenCategories")
    val categories: List<HomeCategoryResponseModel>,
    @SerializedName("mainScreenNewProducts")
    val newProducts: List<HomeProductResponseModel>,
    @SerializedName("mainScreenHitProducts")
    val hitProducts: List<HomeProductResponseModel>,
    @SerializedName("customerCart")
    val cart: CartNetworkModel? = null,
    @SerializedName("cart")
    val anonymousCart: CartNetworkModel? = null,
    @SerializedName("customer")
    val customer: CustomerNetworkModel? = null,
    @SerializedName("blogPostList")
    val blogPostList: List<BlogResponseModel>
)

data class ItemsNetworkModel(
    @SerializedName("items")
    val items: List<CartProductsNetworkModel>
)

data class CustomerNetworkModel(
    @SerializedName("wishlist")
    val wishList: ItemsNetworkModel
)

data class CartNetworkModel(
    @SerializedName("items")
    val products: List<CartProductsNetworkModel>
)

data class CartProductsNetworkModel(
    @SerializedName("id")
    val id: Int,
    @SerializedName("quantity")
    val count: Int = 0,
    @SerializedName("product")
    val product: HomeProductResponseModel
)