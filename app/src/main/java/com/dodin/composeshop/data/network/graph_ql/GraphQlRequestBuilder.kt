package com.dodin.composeshop.data.network.graph_ql

import com.google.gson.Gson
import java.lang.StringBuilder

class GraphQlRequestBuilder(private var requestBody: String? = null, private var request: String = "") {

    companion object {

        private fun <T> convertToRequestString(requestBody: T): String {
            val jsonString = Gson().toJson(requestBody).toString()
            val stringBuilder = StringBuilder(jsonString)
            var charBefore = ""
            var charAfter = ""
            stringBuilder.deleteCharAt(0)
            stringBuilder.deleteCharAt(stringBuilder.lastIndex)
            stringBuilder.forEachIndexed{ index, char ->
                if (stringBuilder.lastIndex > index) {
                    charAfter = stringBuilder[index + 1].toString()
                }
                if (charAfter == ":") stringBuilder.deleteCharAt(index)
                if (index == 0 || charBefore == ",") stringBuilder.deleteCharAt(index)
                charBefore = char.toString()
            }
            val resultStr = stringBuilder.toString()
            stringBuilder.clear()
            return resultStr
        }

        /**@param requestBody - must to be object**/
        fun <T> createBuilder(requestBody: T, requestString: String): GraphQlRequestBuilder {
            val requestBodyString = convertToRequestString(requestBody)
            return GraphQlRequestBuilder(requestBody = requestBodyString, request = requestString)
        }


        fun createBuilder(requestString: String): GraphQlRequestBuilder {
            return GraphQlRequestBuilder(request = requestString)
        }

        fun createBuilder() : GraphQlRequestBuilder {
            return GraphQlRequestBuilder()
        }
    }

    /**@param requestBody - must to be object**/
    fun <T> setRequestModel(requestBody: T) : GraphQlRequestBuilder {
        this.request = convertToRequestString(requestBody)
        return this
    }

    fun setRequest(request : String) : GraphQlRequestBuilder {
        this.request = request
        return this
    }

    fun build(): Map<String, String> {
        return mapOf(GraphQlRequests.QUERY_KEY to String.format(request, requestBody))
    }
}