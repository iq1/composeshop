package com.dodin.composeshop

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import com.dodin.composeshop.core.base.NavigationCommand
import com.dodin.composeshop.core.base.NavigationManager
import com.dodin.composeshop.presentation.authorization.loginGraph
import com.dodin.composeshop.presentation.main.MainScreen
import com.dodin.composeshop.presentation.main.ShowMainScreenRoute
import com.dodin.composeshop.presentation.onboarding.OnBoardingScreen
import com.dodin.composeshop.presentation.onboarding.OnBoardingViewModel
import com.dodin.composeshop.presentation.onboarding.ShowOnBoarding
import com.dodin.composeshop.presentation.product.ProductScreen
import com.dodin.composeshop.presentation.product.ProductViewModel
import com.dodin.composeshop.presentation.product.ShowProductRoute
import com.dodin.composeshop.presentation.webview.ShowWebViewScreen
import com.dodin.composeshop.presentation.webview.WebViewScreen
import com.dodin.composeshop.tools.getViewModel
import org.koin.core.parameter.parametersOf

@Composable
fun NavGraph(
    startDestination: String = ShowOnBoarding.route,
    navigationManager: NavigationManager,
) {
    val navController = rememberNavController()

    navigationManager.handleNavigation(navController)

    NavHost(
        navController = navController,
        startDestination = startDestination
    ) {
        composable(ShowOnBoarding.route) {
            val viewModel = getViewModel<OnBoardingViewModel>(navController)
            OnBoardingScreen(
                viewModel,
                alignment = ShopConfig.onBoardingAlignment,
            )
        }
        composable(ShowMainScreenRoute.route) {
            MainScreen()
        }
        composable(
            ShowProductRoute.route,
            arguments = listOf(
                navArgument("id") { type = NavType.IntType }
            )
        ) {
            val id = it.arguments?.getInt("id")
            val viewModel = getViewModel<ProductViewModel>(navController) {
                parametersOf(id)
            }

            ProductScreen(viewModel)
        }
        loginGraph(navController)
        composable(ShowWebViewScreen.route) { backStackEntry ->
            backStackEntry.arguments?.let {
                val url = it.getString("url").orEmpty()
                val title = it.getString("title").orEmpty()
                WebViewScreen(url, title) {
                    navController.popBackStack()
                }
            }
        }
    }
}

@Composable
private fun NavigationManager.handleNavigation(navController: NavController) {
    LaunchedEffect("navigation") {
        for (command in navCommand) {
            when (command) {
                is NavigationCommand.NavigateTo -> navController.navigate(command.route.destination)
                is NavigationCommand.PopBackStack -> if (command.route == null) {
                    navController.popBackStack()
                } else {
                    navController.popBackStack(command.route.destination, command.inclusive)
                }
            }
        }
    }
}