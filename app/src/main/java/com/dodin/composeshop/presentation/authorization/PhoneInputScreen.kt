package com.dodin.composeshop.presentation.authorization

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dodin.composeshop.Constants
import com.dodin.composeshop.R
import com.dodin.composeshop.ShopConfig
import com.dodin.composeshop.components.CloseButton
import com.dodin.composeshop.components.ModalBottomErrorLayout
import com.dodin.composeshop.components.ShopAppPrimaryButton
import com.dodin.composeshop.theme.GrayRamp
import com.dodin.composeshop.tools.AnnotatedLinkString
import com.dodin.composeshop.tools.MaskTransformation

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun PhoneInputScreen(
    viewModel: UserAuthorizationViewModel,
) {
    val failure = viewModel.failure.observeAsState()
    val bottomSheetState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
    val coroutineScope = rememberCoroutineScope()

    if (failure.value != null) {
        LaunchedEffect("error") {
            bottomSheetState.show()
        }
    }

    ModalBottomErrorLayout(
        failure = failure.value,
        sheetState = bottomSheetState,
        coroutineScope = coroutineScope
    ) {
        ScreenContent(viewModel)
    }
}

@Composable
private fun ScreenContent(viewModel: UserAuthorizationViewModel) {
    var phone by remember { mutableStateOf(viewModel.phone.orEmpty()) }
    var showRules by remember { mutableStateOf(false) }
    val focusRequester = FocusRequester()

    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }

    Column(
        modifier = Modifier.padding(horizontal = 16.dp)
    ) {
        CloseButton(modifier = Modifier.padding(top = 20.dp), onClick = viewModel::onCloseClick)

        Spacer(modifier = Modifier.weight(1f))
        Text(
            text = stringResource(id = R.string.your_number_title),
            style = MaterialTheme.typography.h1,
        )
        OutlinedTextField(
            value = phone,
            singleLine = true,
            onValueChange = { value ->
                phone = value.filter { it.isDigit() }
                if (phone.length == Constants.PHONE_LENGTH) showRules = true
            },
            visualTransformation = MaskTransformation(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            colors = TextFieldDefaults.textFieldColors(
                focusedIndicatorColor = Color.Black,
                unfocusedIndicatorColor = Color.Black,
            ),
            shape = MaterialTheme.shapes.medium,
            modifier = Modifier
                .padding(top = 16.dp)
                .fillMaxWidth()
                .focusRequester(focusRequester),
            keyboardActions = KeyboardActions(
                onDone = { viewModel.sendPhone(phone) }
            )
        )

        ShopAppPrimaryButton(
            text = stringResource(
                if (phone.length == Constants.PHONE_LENGTH) R.string.get_sms_label
                else R.string.enter_phone_number_label
            ),
            enabled = isPhoneValid(phone),
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 45.dp),
            onClick = { viewModel.sendPhone(phone) },
        )
        if (showRules) {
            val annotatedText = AnnotatedLinkString(
                stringResource(R.string.rules_agreement),
                stringResource(R.string.rules_agreement_linkify_string),
                ShopConfig.confidelityUrl,
                MaterialTheme.colors.primary
            )
            val uriHandler = LocalUriHandler.current
            ClickableText(
                text = annotatedText.value,
                style = LocalTextStyle.current.copy(
                    fontSize = 12.sp,
                    color = GrayRamp,
                ),
                modifier = Modifier.padding(top = 10.dp),
                onClick = {
                    annotatedText.getAnnotations(it, it)
                        .firstOrNull()
                        ?.let {
                            uriHandler.openUri(it.item)
                        }
                }
            )
        }
        Spacer(modifier = Modifier.weight(1f))
    }
}

@Preview
@Composable
fun PhoneInputScreenPreview() {
    PhoneInputScreen(UserAuthorizationViewModel())
}

// Move to ViewModel
private fun isPhoneValid(phone: String) = phone.length == Constants.PHONE_LENGTH