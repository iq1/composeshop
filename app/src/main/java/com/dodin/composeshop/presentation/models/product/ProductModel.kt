package com.dodin.composeshop.presentation.models.product

import com.dodin.composeshop.core.base.converters.NetworkConverter
import com.dodin.composeshop.data.Const
import com.dodin.composeshop.data.network.models.products.*
import com.dodin.composeshop.data.network.models.screens.CartNetworkModel
import com.dodin.composeshop.data.network.models.screens.CartProductsNetworkModel
import com.google.gson.Gson
import org.json.JSONObject
import java.io.Serializable
import java.math.BigDecimal
import kotlin.math.roundToInt

data class ProductModel(
    val id: Int,
    val sku: String?,
    val image: String?,
    val name: String?,
    val price: BigDecimal?,
    val currency: String?,
    val specialPrice: BigDecimal? = null,
    val specialPriceCurrency: String? = "",
    val rating: Int = 0,
    val ratingCount: Int = 0,
    var cartCount: Int = 0,
    var cartProductId: Int = 0,
    val labels: List<ProductLabelModel>? = listOf(),
    val isAvailable: Boolean = true,
    val isFavorite: Boolean = false,
    var favoriteProductId: Int = 0,
    val shortDescription: String? = null,
    val description: String? = null,
    val galleryImages: List<String> = emptyList(),
    val reviews: List<ReviewModel> = emptyList(),
    val attributes: List<ProductAttributeModel> = emptyList(),
    val urls: List<String> = emptyList(),
    val crossSellProducts: List<ProductModel> = emptyList(),
    var configurableOptions: List<ConfigurableOption>?,
    val variants: List<Variant>?
) : Serializable {
    companion object : NetworkConverter<HomeProductResponseModel, ProductModel> {
        override fun convertFromNetworkResponse(response: HomeProductResponseModel): ProductModel {
            val specialPriceValue = response.specialPrice
            val priceValue = response.priceRange?.minimumPrice?.regularPrice?.value
            return ProductModel(
                id = response.id,
                sku = response.sku,
                image = response.image?.url,
                name = response.name,
                price = if (priceValue.isNullOrBlank()) null else BigDecimal(priceValue), // TODO: handle minimum and maximum prices
                currency = response.priceRange?.minimumPrice?.regularPrice?.currency,
                specialPrice = if (specialPriceValue.isNullOrBlank()) null else BigDecimal(specialPriceValue),
                specialPriceCurrency = response.priceRange?.minimumPrice?.regularPrice?.currency,
                rating = summaryToRating(response.ratingSummary?.toIntOrNull() ?: 0).roundToInt(),
                ratingCount = response.reviewsCount?.toIntOrNull() ?: 0,
                isAvailable = response.stockStatus == StockStatus.IN_STOCK,
                labels = response.labels?.map {
                    ProductLabelModel(
                        title = it.title,
                        hexColor = JSONObject(it.customStyle).optString("background", "")
                    )

                }.orEmpty(),
                isFavorite = response.isFavorite,
                shortDescription = response.shortDescription?.html,
                description = response.description,
                galleryImages = response.mediaGallery?.filterNot { it.isDisabled }?.map { it.url }
                    .orEmpty(),
                reviews = response.reviews?.map {
                    ReviewModel(
                        name = it.nickname,
                        content = it.text,
                        value = it.rating.firstOrNull()?.value ?: 0,
                        date = it.date,
                    )
                }.orEmpty(),
                attributes = response.attributes?.map {
                    ProductAttributeModel(it.label, it.value)
                }.orEmpty(),
                urls = response.urls?.map { it.url }.orEmpty(),
                crossSellProducts = ProductModel.convertFromNetworkResponse(response.crosssellProducts.orEmpty()),
                configurableOptions = response.configurableOptions?.map {
                    ConfigurableOption.convertFromNetworkResponse(it)
                }?.sortedBy { it.position },
                variants = response.variants?.map(Variant::convertFromNetworkResponse)
            )
        }

        fun convertFromNetworkResponse(
            response: HomeProductResponseModel,
            wishList: List<CartProductsNetworkModel>,
            wishListSkus: List<String>,
            cart: CartNetworkModel? = null,
        ): ProductModel {
            val productInCart = cart?.products?.find { it.product.sku == response.sku }
            val cartCount = productInCart?.count ?: 0
            val cartProductId = productInCart?.id ?: 0
            val favoriteProductId = wishList.find { it.product.sku == response.sku }?.id ?: -1

            return convertFromNetworkResponse(response)
                .copy(
                    isFavorite = wishListSkus.contains(response.sku),
                    favoriteProductId = favoriteProductId,
                    cartCount = cartCount,
                    cartProductId = cartProductId,
                )
        }

        fun convertFromNetworkResponse(
            response: List<HomeProductResponseModel>,
            wishList: List<CartProductsNetworkModel>,
            cart: CartNetworkModel? = null,
        ): List<ProductModel> {
            val wishListSkus = wishList.mapNotNull { it.product.sku }
            return response.map { convertFromNetworkResponse(it, wishList, wishListSkus, cart) }
        }

        private fun summaryToRating(summary: Int): Float =
            Const.DefaultConst.MAX_RATING_STARS / 100f * summary
    }
}

data class ConfigurableOption(
    val id: Int,
    val attributeId: String,
    val label: String,
    val position: Int,
    val useDefault: Boolean,
    val attributeCode: String,
    val productId: Int,
    val values: List<ConfigurableOptionsValue>?
) {
    companion object : NetworkConverter<ConfigurableOptionResponseModel, ConfigurableOption> {
        override fun convertFromNetworkResponse(response: ConfigurableOptionResponseModel): ConfigurableOption {
            return ConfigurableOption(
                id = response.id,
                attributeId = response.attributeId,
                label = response.label,
                position = response.position,
                useDefault = response.useDefault,
                attributeCode = response.attributeCode,
                productId = response.productId,
                values = response.values?.map(ConfigurableOptionsValue::convertFromNetworkResponse)
            )
        }
    }
}

data class ConfigurableOptionsValue(
    val valueIndex: Int,
    val label: String,
    val swatchData: SwatchData?
) {
    companion object :
        NetworkConverter<ConfigurableOptionsValueResponseModel, ConfigurableOptionsValue?> {
        override fun convertFromNetworkResponse(response: ConfigurableOptionsValueResponseModel): ConfigurableOptionsValue {
            return ConfigurableOptionsValue(
                valueIndex = response.valueIndex,
                label = response.label,
                swatchData = response.swatchData?.let(SwatchData::convertFromNetworkResponse)
            )
        }
    }
}

data class SwatchData(
    val value: String
) {
    companion object : NetworkConverter<SwatchDataResponseModel, SwatchData> {
        override fun convertFromNetworkResponse(response: SwatchDataResponseModel): SwatchData {
            return SwatchData(
                value = response.value
            )
        }
    }
}

data class Variant(
    val attributes: List<VariantAttribute>?,
    val product: ProductModel?
) {
    companion object : NetworkConverter<VariantResponseModel, Variant> {
        override fun convertFromNetworkResponse(response: VariantResponseModel): Variant {
            return Variant(
                attributes = response.attributes?.map(VariantAttribute::convertFromNetworkResponse),
                product = response.product?.let(ProductModel::convertFromNetworkResponse)
            )
        }
    }
}

data class VariantAttribute(
    val code: String,
    val label: String,
    val valueIndex: Int,
    var available: Boolean = false,
    var subVariants: List<VariantAttribute>? = null,
    var product: ProductModel? = null
) {
    companion object :
        NetworkConverter<VariantAttributeResponseModel, VariantAttribute> {
        override fun convertFromNetworkResponse(
            response: VariantAttributeResponseModel
        ): VariantAttribute {
            return VariantAttribute(
                code = response.code,
                label = response.label,
                valueIndex = response.valueIndex
            )
        }

    }

    fun deepCopy(): VariantAttribute {
        val json = Gson().toJson(this)
        return Gson().fromJson(json, VariantAttribute::class.java)
    }
}