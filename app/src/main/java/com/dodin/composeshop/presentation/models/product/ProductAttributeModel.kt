package com.dodin.composeshop.presentation.models.product

data class ProductAttributeModel(
    val title: String,
    val value: String,
)