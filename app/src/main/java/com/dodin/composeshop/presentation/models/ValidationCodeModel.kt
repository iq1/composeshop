package com.dodin.composeshop.presentation.models

import com.dodin.composeshop.core.base.converters.NetworkConverter
import com.dodin.composeshop.data.network.models.registration.ValidateSmsCodeResponse


data class ValidationCodeModel(val success: Boolean, val token: String, val secret: String) {

    companion object : NetworkConverter<ValidateSmsCodeResponse, ValidationCodeModel> {
        override fun convertFromNetworkResponse(response: ValidateSmsCodeResponse):
                ValidationCodeModel {
            val data = response.validateSmsCodeResponseModel
            return ValidationCodeModel(
                data.success,
                data.customerToken.token,
                data.customerToken.secret
            )
        }
    }
}