package com.dodin.composeshop.presentation.webview

import com.dodin.composeshop.core.base.Route
import com.dodin.composeshop.tools.replace
import java.net.URLEncoder

class ShowWebViewScreen(private val url: String, private val title: String) : Route {
    override val destination: String
        get() = route
            .replace(
                "{url}" to URLEncoder.encode(url, "utf-8"),
                "{title}" to title
            )

    companion object {
        const val route = "webview/{url}/{title}"
    }
}