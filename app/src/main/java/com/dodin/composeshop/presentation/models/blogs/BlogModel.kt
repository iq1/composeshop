package com.dodin.composeshop.presentation.models.blogs

import com.dodin.composeshop.core.base.converters.NetworkConverter
import com.dodin.composeshop.data.network.models.blog.BlogResponseModel

data class BlogModel(
    val entityId: Int?,
    val title: String?,
    val urlKey: String?,
    val fullUrl: String?,
    val description: String?,
    val categoryIds: List<Int>?,
    val contentNavMenu: String?,
    val author: String?,
    val content: String?,
    val previewImage: String?,
    val image: String?,
    val createdAt: String?,
    val updatedAt: String?
) {
    companion object : NetworkConverter<BlogResponseModel, BlogModel> {
        override fun convertFromNetworkResponse(response: BlogResponseModel): BlogModel {
            return BlogModel(
                entityId = response.entityId,
                title = response.title,
                urlKey = response.urlKey,
                fullUrl = response.fullUrl,
                description = response.description,
                categoryIds = response.categoryIds,
                contentNavMenu = response.contentNavMenu,
                author = response.author,
                content = response.content,
                previewImage = response.previewImage,
                image = response.image,
                createdAt = response.createdAt,
                updatedAt = response.updatedAt
            )
        }
    }
}