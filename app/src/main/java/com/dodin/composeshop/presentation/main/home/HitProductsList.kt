package com.dodin.composeshop.presentation.main.home

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.dodin.composeshop.components.ProductHorizontal
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.dodin.composeshop.theme.GrayRampLightenest

@Composable
fun HitProductsList(
    products: List<ProductModel>,
    onProductClick: (ProductModel) -> Unit,
    onFavoriteClick: (ProductModel) -> Unit,
    onCartClick: (ProductModel) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 24.dp, start = 8.dp, end = 8.dp),
    ) {
        products.forEachIndexed { index, product ->
            ProductHorizontal(product, onProductClick, onFavoriteClick, onCartClick)

            if (index < products.size - 1) Divider(
                color = GrayRampLightenest,
                thickness = 1.dp,
                modifier = Modifier.padding(vertical = 4.dp)
            )
        }
    }
}

@Preview
@Composable
fun HitProductsListPreview() {
    HitProductsList(mockNewProducts, {}, {}, {})
}