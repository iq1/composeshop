package com.dodin.composeshop.presentation.authorization

import com.dodin.composeshop.core.base.Route

class ShowAuthorizationScreen : Route {
    override val destination: String
        get() = route

    companion object {
        const val route = "auth"
    }
}

class ShowPhoneScreen : Route {
    override val destination: String
        get() = route

    companion object {
        const val route = "phone"
    }
}

class ShowOtpInputScreen : Route {
    override val destination: String
        get() = route

    companion object {
        const val route = "smscode"
    }
}

class ShowUserDataInputScreen : Route {
    override val destination: String
        get() = route

    companion object {
        const val route = "userdatainput"
    }
}

class ShowSuccessScreen : Route {
    override val destination: String
        get() = route

    companion object {
        const val route = "success"
    }
}