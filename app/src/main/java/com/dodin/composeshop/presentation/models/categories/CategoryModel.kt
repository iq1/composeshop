package com.dodin.composeshop.presentation.models.categories

import com.dodin.composeshop.core.base.converters.NetworkConverter
import com.dodin.composeshop.data.network.models.categories.HomeCategoryResponseModel

data class CategoryModel(
    val id: Int,
    val key: String,
    val image: String,
    val mobileImage: String,
    val name: String,
    val position: Int,
    val level: Int,
) {
    companion object: NetworkConverter<HomeCategoryResponseModel, CategoryModel> {
        override fun convertFromNetworkResponse(response: HomeCategoryResponseModel): CategoryModel {
            return CategoryModel(
                id = response.id,
                key = response.urlKey,
                image = response.image,
                mobileImage = response.mobileImage,
                name = response.name,
                position = response.position,
                level = response.level,
            )
        }
    }
}