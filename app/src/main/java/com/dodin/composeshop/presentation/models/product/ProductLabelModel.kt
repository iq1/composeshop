package com.dodin.composeshop.presentation.models.product

data class ProductLabelModel(
    val title: String,
    val hexColor: String,
)