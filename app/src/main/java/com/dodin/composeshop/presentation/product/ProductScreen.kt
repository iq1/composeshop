package com.dodin.composeshop.presentation.product

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.text.HtmlCompat
import coil.compose.rememberImagePainter
import com.dodin.composeshop.R
import com.dodin.composeshop.components.*
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.dodin.composeshop.theme.*
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState

@Composable
fun ProductScreen(
    viewModel: ProductViewModel,
) {
    val product = viewModel.productLiveData.observeAsState()
    val scrollState = rememberScrollState()

    Scaffold(
        topBar = { TopBar(product.value, viewModel::onBackButtonClick, viewModel::onShareButtonClick) }
    ) { paddingValues ->
        Column(
            modifier = Modifier
                .verticalScroll(scrollState)
                .padding(paddingValues)
        ) {
            if (product.value == null) SimpleProgress()
            else Product(product.value!!, viewModel::onFavoriteClick, viewModel::onViewAllAttributesClick)

        }
    }
}

@Composable
private fun TopBar(
    product: ProductModel?,
    onBackButtonClick: () -> Unit,
    onShareButtonClick: (ProductModel) -> Unit,
) {
    TopAppBar(
        backgroundColor = Color.White,
        elevation = 0.dp,
    ) {
        BackButton(onClick = onBackButtonClick)
        Spacer(Modifier.weight(1f))
        if (product != null) ShareButton { onShareButtonClick(product) }
    }
}

@OptIn(ExperimentalPagerApi::class)
@Composable
private fun Product(
    product: ProductModel,
    onFavoriteClick: (ProductModel) -> Unit,
    onViewAllAttributesClick: (ProductModel) -> Unit,
) {
    Box(
        modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 12.dp)
    ) {
        val pagerState = rememberPagerState(pageCount = product.galleryImages.size)

        HorizontalPager(
            modifier = Modifier.fillMaxWidth(),
            state = pagerState
        ) { page ->
            val painter = rememberImagePainter(
                data = product.galleryImages[page],
            )
            Image(
                painter = painter,
                contentScale = ContentScale.FillHeight,
                contentDescription = product.name,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp)
            )
        }

        ProductLabelsList(product.labels.orEmpty())
    }

    Text(
        text = product.name.orEmpty(),
        style = MaterialTheme.typography.h2,
        maxLines = 2,
        modifier = Modifier.padding(start = 24.dp, end = 24.dp, top = 45.dp),
    )

    Row(modifier = Modifier.padding(top = 12.dp, start = 16.dp, end = 16.dp)) {
        Text(stringResource(R.string.product_code), color = Gray900)
        Text(product.sku.orEmpty(), maxLines = 4, modifier = Modifier.padding(start = 4.dp))
        Spacer(Modifier.weight(1f))
        ProductRating(product)
    }

    ShopAppSecondaryButton(
        text = stringResource(R.string.add_to_favorites),
        icon = {
            FavoriteIcon(
                isFavorite = product.isFavorite,
                tint = GrayRampLightenest
            )
        },
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 28.dp, start = 24.dp, end = 24.dp),
        onClick = { onFavoriteClick(product) }
    )

    Characteristics(product, onViewAllAttributesClick)
    Divider()
    Description(product)
    Divider()
    TextWithArrow(
        stringResource(R.string.product_delivery),
        modifier = Modifier.padding(top = 28.dp, start = 24.dp, end = 24.dp)
    ) {
    }
    Divider()
    if (product.reviews.isEmpty()) NoReviews()
    else Reviews(product)

    CartButton(
        product, modifier = Modifier
            .fillMaxWidth()
            .padding(top = 50.dp, start = 24.dp, end = 24.dp)
    )
    Spacer(Modifier.height(20.dp))
}

@Composable
private fun Characteristics(product: ProductModel, onViewAllClick: (ProductModel) -> Unit) {
    Collapsable(
        title = stringResource(R.string.product_characteristics),
        titleStyle = MaterialTheme.typography.h2,
        isOpen = true,
        modifier = Modifier.padding(start = 24.dp, end = 24.dp, top = 50.dp)
    ) {
        Column(
            verticalArrangement = Arrangement.spacedBy(20.dp),
            modifier = Modifier.padding(top = 20.dp)
        ) {
            product.attributes.take(5).forEach { attribute ->
                Row {
                    Text(text = attribute.title, color = GrayRamp)
                    Spacer(Modifier.weight(1f))
                    Text(text = attribute.value, fontWeight = FontWeight.Bold)
                }
            }

            TextWithArrow(stringResource(R.string.product_all_characteristics)) {
                onViewAllClick(product)
            }
        }
    }
}

@Composable
private fun Description(product: ProductModel) {
    Collapsable(
        title = stringResource(R.string.product_description),
        titleStyle = MaterialTheme.typography.h3,
        isOpen = false,
        modifier = Modifier.padding(start = 24.dp, end = 24.dp, top = 50.dp)
    ) {
        val text =
            HtmlCompat.fromHtml(product.description.orEmpty(), HtmlCompat.FROM_HTML_MODE_LEGACY)
                .toString()
        Text(text = text, maxLines = 5, color = GrayRamp)
        Spacer(Modifier.height(20.dp))
        TextWithArrow(stringResource(R.string.read_more)) {

        }
    }
}

@Composable
private fun NoReviews() {
    Column(modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 55.dp)) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .size(45.dp)
                    .background(PressedYellow, CircleShape)
            ) {
                Icon(
                    painter = painterResource(R.drawable.chat_bubble),
                    contentDescription = null,
                )
            }

            Text(
                text = stringResource(R.string.no_reviews),
                style = MaterialTheme.typography.h2,
                color = Gray950,
                modifier = Modifier.padding(start = 18.dp)
            )
        }

        Text(
            text = stringResource(R.string.no_reviews_subtitle),
            color = Gray900,
            modifier = Modifier.padding(top = 24.dp)
        )

        Row(
            modifier = Modifier
                .padding(top = 25.dp)
                .align(Alignment.End)
        ) {
            Icon(
                painter = painterResource(R.drawable.chat_bubble),
                contentDescription = null,
            )
            Text(
                text = stringResource(R.string.product_create_review),
                color = Gray900,
                fontSize = 15.sp,
                modifier = Modifier.padding(start = 7.dp)
            )
        }
    }
}

@Composable
private fun Reviews(product: ProductModel) {
    // TODO
}

@Composable
private fun TextWithArrow(text: String, modifier: Modifier = Modifier, onClick: () -> Unit) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier.clickable { onClick() }
    ) {
        Text(text = text, fontWeight = FontWeight.Bold)
        Icon(
            imageVector = Icons.Default.ArrowForward,
            contentDescription = null,
            modifier = Modifier
                .padding(start = 12.dp)
                .size(18.dp)
        )
    }
}

@Composable
private fun Divider() {
    Divider(modifier = Modifier.padding(top = 28.dp, start = 24.dp, end = 24.dp))
}

@Composable
private fun SimpleProgress() {
    Box {
        CircularProgressIndicator()
    }
}

@Preview
@Composable
fun ProductScreenPreview() {
    ProductScreen(ProductViewModel(1))
}