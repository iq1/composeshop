package com.dodin.composeshop.presentation.product

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.dodin.composeshop.core.base.NavigationManager
import com.dodin.composeshop.data.network.repository.ProductRepository
import com.dodin.composeshop.presentation.models.product.ProductModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ua.brander.core.viewmodel.BaseViewModel

class ProductViewModel(private val productId: Int) : BaseViewModel(), KoinComponent {
    private val navigationManager: NavigationManager by inject()
    private val productRepository: ProductRepository by inject()

    val productLiveData: MutableLiveData<ProductModel> = MutableLiveData()

    init {
        loadProduct()
    }

    fun onBackButtonClick() = viewModelScope.launch {
        navigationManager.popBackStack()
    }

    fun onShareButtonClick(product: ProductModel) {

    }

    fun onFavoriteClick(product: ProductModel) {

    }

    fun onViewAllAttributesClick(product: ProductModel) {

    }

    private fun loadProduct() {
        viewModelScope.launch(Dispatchers.IO) {
            productRepository.getFullProduct(productId).either(failure::postValue, ::handleProduct)
        }
    }

    private fun handleProduct(newProductModel: ProductModel) {
        productLiveData.postValue(newProductModel)
    }
}