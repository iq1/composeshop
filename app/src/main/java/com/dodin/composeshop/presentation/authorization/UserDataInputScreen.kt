package com.dodin.composeshop.presentation.authorization

import android.util.Patterns
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.dodin.composeshop.R
import com.dodin.composeshop.components.CloseButton
import com.dodin.composeshop.components.OutlinedTextFieldWithError
import com.dodin.composeshop.components.ShopAppPrimaryButton

@Composable
fun UserDataInputScreen(
    viewModel: UserAuthorizationViewModel
) {
    val focusRequester = FocusRequester()

    var name by remember { mutableStateOf("") }
    var surname by remember { mutableStateOf("") }
    var email by remember { mutableStateOf("") }

    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }

    Column(
        modifier = Modifier
            .fillMaxHeight()
            .padding(horizontal = 24.dp)
    ) {
        CloseButton(modifier = Modifier.padding(top = 20.dp), onClick = viewModel::onCloseClick)

        Text(
            text = stringResource(R.string.your_data_string),
            style = MaterialTheme.typography.h1,
            modifier = Modifier.padding(top = 8.dp)
        )

        UserInputTextField(
            value = name,
            onTextChange = {
                if (isPersonalFieldValidSymbols(it)) name = it
            },
            validator = { !isPersonFieldValid(name) },
            label = stringResource(R.string.name_string),
            error = stringResource(R.string.not_correct_name),
            modifier = Modifier.padding(top = 40.dp)
                .focusRequester(focusRequester),
        )
        UserInputTextField(
            value = surname,
            onTextChange = { surname = it },
            validator = { !isPersonFieldValid(surname) },
            label = stringResource(R.string.surname_string),
            error = stringResource(R.string.not_correct_surname),
            modifier = Modifier.padding(top = 36.dp)
        )
        UserInputTextField(
            value = email,
            onTextChange = {
                email = it
            },
            label = stringResource(R.string.not_correct_email_error_text),
            error = stringResource(R.string.not_correct_email_error_text),
            validator = { !isEmailFieldValid(email) },
            modifier = Modifier.padding(top = 36.dp)
        )

        Spacer(Modifier.weight(1f))
        ShopAppPrimaryButton(
            text = stringResource(R.string.save_string),
            enabled = isDataValid(name, surname, email),
            onClick = { viewModel.saveUserData(name, surname, email) },
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 24.dp)
        )
    }
}

@Composable
private fun ColumnScope.UserInputTextField(
    value: String,
    onTextChange: (String) -> Unit,
    label: String,
    error: String,
    validator: (String) -> Boolean,
    modifier: Modifier = Modifier,
) {
    OutlinedTextFieldWithError(
        value = value,
        onValueChange = onTextChange,
        isError = validator(value),
        error = {
            Text(
                error,
                color = MaterialTheme.colors.error,
                modifier = Modifier.align(Alignment.End)
            )
        },
        label = { Text(label) },
        singleLine = true,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email),
        shape = MaterialTheme.shapes.medium,
        modifier = modifier
            .fillMaxWidth(),
    )
}

@Preview
@Composable
fun UserDataInputScreenPreview() {
    UserDataInputScreen(UserAuthorizationViewModel())
}

// TODO: move to viewmodel
private fun isDataValid(name: String, surname: String, email: String) = isPersonFieldValid(name) &&
        isPersonFieldValid(surname) &&
        isEmailFieldValid(email)

private fun isEmailFieldValid(email: String) =
    Patterns.EMAIL_ADDRESS.matcher(email).matches()

private fun isPersonFieldValid(value: String) =
    Regex("[a-zA-Zа-яА-я']{2,}").matches(value)

private fun isPersonalFieldValidSymbols(value: String) =
    Regex("[a-zA-Zа-яА-я']*").matches(value)