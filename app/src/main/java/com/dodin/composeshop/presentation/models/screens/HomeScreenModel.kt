package com.dodin.composeshop.presentation.models.screens

import com.dodin.composeshop.core.base.converters.NetworkConverter
import com.dodin.composeshop.data.network.models.screens.HomeScreenResponseModel
import com.dodin.composeshop.presentation.models.categories.CategoryModel
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.dodin.composeshop.presentation.models.banners.BannersModel
import com.dodin.composeshop.presentation.models.blogs.BlogModel

data class HomeScreenModel(
    val banners: BannersModel,
    val categories: List<CategoryModel>,
    val newProducts: List<ProductModel>,
    val hitProducts: List<ProductModel>,
    val blogs: List<BlogModel>,
) {
    companion object : NetworkConverter<HomeScreenResponseModel, HomeScreenModel> {
        override fun convertFromNetworkResponse(response: HomeScreenResponseModel): HomeScreenModel {
            return HomeScreenModel(
                banners = BannersModel.convertFromNetworkResponse(response.banners),
                categories = CategoryModel.convertFromNetworkResponse(response.categories),
                newProducts = ProductModel.convertFromNetworkResponse(
                    response.newProducts,
                    response.customer?.wishList?.items.orEmpty(),
                    response.cart ?: response.anonymousCart,
                ),
                hitProducts = ProductModel.convertFromNetworkResponse(
                    response.hitProducts,
                    response.customer?.wishList?.items.orEmpty(),
                    response.cart ?: response.anonymousCart,
                ),
                blogs = BlogModel.convertFromNetworkResponse(response.blogPostList),
            )
        }
    }
}