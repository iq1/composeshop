package com.dodin.composeshop.presentation.models

import com.dodin.composeshop.core.base.converters.NetworkConverter
import com.dodin.composeshop.data.network.models.registration.ResendSmsResponse

data class ResendCodeModel(
    val message: String = "",
    val success: Boolean = true,
    val canShowResend: Boolean = true
) {
    companion object : NetworkConverter<ResendSmsResponse, ResendCodeModel> {
        override fun convertFromNetworkResponse(response: ResendSmsResponse): ResendCodeModel {
            val data = response.resendSmsResponseModel
            return ResendCodeModel(data.message, data.success, data.canShowResend)
        }
    }
}