package com.dodin.composeshop.presentation.main

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.dodin.composeshop.R
import com.dodin.composeshop.presentation.main.home.*
import com.dodin.composeshop.tools.getViewModel

@Composable
fun MainScreen() {
    val navController = rememberNavController()

    Scaffold(
        bottomBar = {
            BottomNavigation(backgroundColor = Color.White) {
                val navBackStackEntry by navController.currentBackStackEntryAsState()
                val currentDestination = navBackStackEntry?.destination

                bottomNavigationItems.forEach { item ->
                    BottomNavigationItem(
                        icon = { getIcon(item) },
                        label = { Text(stringResource(item.titleResId)) },
                        selected = currentDestination?.hierarchy?.any { it.route == item.route } == true,
                        onClick = {
                            navController.navigate(item.route) {
                                // Pop up to the start destination of the graph to
                                // avoid building up a large stack of destinations
                                // on the back stack as users select items
                                popUpTo(navController.graph.findStartDestination().id) {
                                    saveState = true
                                }
                                // Avoid multiple copies of the same destination when
                                // reselecting the same item
                                launchSingleTop = true
                                // Restore state when reselecting a previously selected item
                                restoreState = true
                            }
                        }
                    )
                }
            }
        }
    ) { innerPadding ->
        val scrollState = rememberScrollState()
        val blogScrollState = rememberLazyListState()
        val newScrollState = rememberLazyListState()

        NavHost(navController, startDestination = MainTabsRoutes.home, Modifier.padding(innerPadding)) {
            composable(MainTabsRoutes.home) {
                val viewModel = getViewModel<HomeViewModel>(navController)
                HomeScreen(viewModel, scrollState, blogScrollState, newScrollState)
            }
            composable(MainTabsRoutes.catalog) {
                StubScreen(MainTabsRoutes.catalog)
            }
            composable(MainTabsRoutes.cart) {
                StubScreen(MainTabsRoutes.cart)
            }
            composable(MainTabsRoutes.favorites) {
                StubScreen(MainTabsRoutes.favorites)
            }
            composable(MainTabsRoutes.other) {
                StubScreen(MainTabsRoutes.other)
            }
        }
    }
}

@Composable
private fun getIcon(item: NavigationItem) =
    when (item.icon) {
        // TODO: implement it
        is IconResource.Downloadable -> Icon(
            imageVector = Icons.Default.Check,
            contentDescription = null
        )
        is IconResource.Resource -> Icon(
            painter = painterResource(item.icon.drawableResId),
            contentDescription = null
        )
    }


val bottomNavigationItems = arrayOf(
    NavigationItem(
        MainTabsRoutes.home,
        R.string.navigation_main,
        IconResource.Resource(R.drawable.custom_shop_home)
    ),
    NavigationItem(
        MainTabsRoutes.catalog,
        R.string.navigation_catalog,
        IconResource.Resource(R.drawable.custom_shop_catalog)
    ),
    NavigationItem(
        MainTabsRoutes.cart,
        R.string.navigation_cart,
        IconResource.Resource(R.drawable.custom_shop_product_button_cart)
    ),
    NavigationItem(
        MainTabsRoutes.favorites,
        R.string.navigation_favorites,
        IconResource.Resource(R.drawable.custom_shop_favorites)
    ),
    NavigationItem(
        MainTabsRoutes.other,
        R.string.navigation_other,
        IconResource.Resource(R.drawable.custom_shop_other)
    ),
)

object MainTabsRoutes {
    const val home = "home"
    const val catalog = "catalog"
    const val cart = "cart"
    const val favorites = "favorites"
    const val other = "other"
}