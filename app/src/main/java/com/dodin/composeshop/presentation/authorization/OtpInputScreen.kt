package com.dodin.composeshop.presentation.authorization

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dodin.composeshop.R
import com.dodin.composeshop.components.CloseButton
import com.dodin.composeshop.components.ShopAppPrimaryButton
import com.dodin.composeshop.theme.GrayRamp
import com.dodin.composeshop.tools.formatWithMask
import kotlinx.coroutines.delay

@Composable
fun OtpInputScreen(
    viewModel: UserAuthorizationViewModel,
) {
    val focusRequester = FocusRequester()
    val otp by viewModel.otp.observeAsState("")
    val resendCode by viewModel.resendPhoneLiveData.observeAsState()
    val validateCode by viewModel.validateCodeLiveData.observeAsState()

    // TODO: move timer to viewmodel
    var timer by remember { mutableStateOf(DEFAULT_TIMER_SEC) }

    LaunchedEffect(Unit) {
        focusRequester.requestFocus()

        for (i in DEFAULT_TIMER_SEC downTo 0) {
            timer = i
            delay(1000)
        }
    }

    Column(
        modifier = Modifier
            .fillMaxHeight()
            .padding(horizontal = 24.dp)
    ) {
        CloseButton(modifier = Modifier.padding(top = 20.dp), onClick = viewModel::onCloseClick)

        Spacer(Modifier.weight(1f))
        Text(
            text = stringResource(R.string.code_from_sms_label),
            style = MaterialTheme.typography.h1,
        )
        val otpFieldColors = if (validateCode?.success != false) {
            TextFieldDefaults.textFieldColors(
                focusedIndicatorColor = Color.Black,
                unfocusedIndicatorColor = Color.Black,
            )
        } else {
            TextFieldDefaults.textFieldColors(
                focusedIndicatorColor = Color.Red,
                unfocusedIndicatorColor = Color.Red,
            )
        }
        OutlinedTextField(
            value = otp,
            onValueChange = { viewModel.otp.value = it.take(4) },
            singleLine = true,
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            colors = otpFieldColors,
            shape = MaterialTheme.shapes.medium,
            textStyle = LocalTextStyle.current.copy(
                textAlign = TextAlign.Center,
                letterSpacing = 16.sp
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp)
                .focusRequester(focusRequester),
            keyboardActions = KeyboardActions(
                onDone = { viewModel.validateCode(otp) }
            )
        )
        Row(modifier = Modifier.padding(top = 8.dp)) {
            Text(
                text = stringResource(R.string.code_sent_to_number_label),
                color = GrayRamp,
                modifier = Modifier.padding(end = 2.dp)
            )
            Text(viewModel.phone.orEmpty().formatWithMask("(###) ### ## ##"))
        }

        ShopAppPrimaryButton(
            text = stringResource(R.string.sign_in_string),
            enabled = otp.length == 4,
            onClick = { viewModel.validateCode(otp) },
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 30.dp)
        )
        ShopAppPrimaryButton(
            text = if (timer == 0) stringResource(R.string.resend_sms_code_string)
            else stringResource(
                R.string.resend_code_after_string,
                timer.toString().padStart(2, '0')
            ),
            enabled = timer == 0 && resendCode?.canShowResend == true,
            onClick = {
                viewModel.resendCode()
                timer = DEFAULT_TIMER_SEC
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp)
        )
        Spacer(Modifier.weight(1f))
    }
}

private const val DEFAULT_TIMER_SEC = 60

@Preview
@Composable
fun OtpInputScreenPreview () {
    OtpInputScreen(UserAuthorizationViewModel())
}