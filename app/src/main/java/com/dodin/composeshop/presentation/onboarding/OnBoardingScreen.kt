package com.dodin.composeshop.presentation.onboarding

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dodin.composeshop.OnBoardingAlignment
import com.dodin.composeshop.R
import com.dodin.composeshop.components.ShopAppPrimaryButton
import com.dodin.composeshop.components.ShopAppSecondaryButton
import com.dodin.composeshop.theme.Gray292

@Composable
fun OnBoardingScreen(
    viewmodel: OnBoardingViewModel,
    alignment: OnBoardingAlignment,
) {
    when (alignment) {
        OnBoardingAlignment.CENTER -> OnBoardingCenterAlignmentScreen(
            viewmodel::onSkipClick,
            viewmodel::onAuthClick
        )
        OnBoardingAlignment.LEFT -> OnBoardingLeftAlignmentScreen(
            viewmodel::onSkipClick,
            viewmodel::onAuthClick
        )
    }
}

@Composable
fun OnBoardingCenterAlignmentScreen(
    onSkipClick: () -> Unit,
    onAuthClick: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Spacer(modifier = Modifier.weight(1f))
        Image(
            painter = painterResource(R.drawable.custom_shop_onboarding_logo_placeholder),
            contentDescription = "logo"
        )
        Spacer(modifier = Modifier.height(45.dp))

        Text(
            text = stringResource(id = R.string.welcome_title_text_label),
            fontSize = 28.sp,
            fontWeight = FontWeight.Bold,
            color = Gray292,
            modifier = Modifier.padding(horizontal = 32.dp),
        )
        Text(
            text = stringResource(id = R.string.welcome_message_text_label),
            fontSize = 16.sp,
            fontWeight = FontWeight.Medium,
            color = Gray292,
            modifier = Modifier.padding(horizontal = 32.dp),
        )

        Spacer(modifier = Modifier.weight(1f))
        ShopAppSecondaryButton(
            text = stringResource(id = R.string.skip_string),
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 24.dp),
            onClick = onSkipClick
        )
        Spacer(modifier = Modifier.height(12.dp))
        ShopAppPrimaryButton(
            text = stringResource(id = R.string.sign_in_string),
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 24.dp),
            onClick = onAuthClick
        )
        Spacer(modifier = Modifier.height(12.dp))
    }
}

@Composable
fun OnBoardingLeftAlignmentScreen(
    onSkipClick: () -> Unit,
    onAuthClick: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
    ) {
        Image(
            painter = painterResource(R.drawable.custom_shop_onboarding_logo_placeholder),
            contentDescription = "logo",
            modifier = Modifier.padding(
                top = 55.dp,
                start = 24.dp,
            )
        )
        Spacer(modifier = Modifier.weight(1f))

        Text(
            text = stringResource(id = R.string.welcome_title_text_label),
            fontSize = 28.sp,
            fontWeight = FontWeight.Bold,
            color = Gray292,
            modifier = Modifier.padding(horizontal = 32.dp),
        )
        Text(
            text = stringResource(id = R.string.welcome_message_text_label),
            fontSize = 16.sp,
            fontWeight = FontWeight.Medium,
            color = Gray292,
            modifier = Modifier.padding(horizontal = 32.dp),
        )
        Spacer(modifier = Modifier.height(50.dp))

        ShopAppSecondaryButton(
            text = stringResource(id = R.string.skip_string),
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 24.dp),
            onClick = onSkipClick
        )
        Spacer(modifier = Modifier.height(12.dp))
        ShopAppPrimaryButton(
            text = stringResource(id = R.string.sign_in_string),
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 24.dp),
            onClick = onAuthClick
        )
        Spacer(modifier = Modifier.height(12.dp))
    }
}

@Preview
@Composable
fun OnBoardingScreenPreview(@PreviewParameter(AlignmentProvider::class) alignment: OnBoardingAlignment) {
    OnBoardingScreen(OnBoardingViewModel(), alignment)
}

class AlignmentProvider : PreviewParameterProvider<OnBoardingAlignment> {
    override val values = sequenceOf(OnBoardingAlignment.CENTER, OnBoardingAlignment.LEFT)
}