package com.dodin.composeshop.presentation.authorization

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.dodin.composeshop.core.base.NavigationManager
import com.dodin.composeshop.data.network.models.registration.ResendSmsRequestModel
import com.dodin.composeshop.data.network.models.registration.SendPhoneRequestModel
import com.dodin.composeshop.data.network.models.registration.ValidateSmsCodeRequestModel
import com.dodin.composeshop.data.network.repository.RegistrationRepository
import com.dodin.composeshop.data.storage.AppPreferences
import com.dodin.composeshop.presentation.main.ShowMainScreenRoute
import com.dodin.composeshop.presentation.models.IsNewUserWithHashModel
import com.dodin.composeshop.presentation.models.ResendCodeModel
import com.dodin.composeshop.presentation.models.ValidationCodeModel
import com.dodin.composeshop.presentation.onboarding.ShowOnBoarding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ua.brander.core.viewmodel.BaseViewModel

class UserAuthorizationViewModel : BaseViewModel(), KoinComponent {
    private val navigationManager: NavigationManager by inject()
    private val appPreferences: AppPreferences by inject()
    private val registrationRepository: RegistrationRepository by inject()

    val sendPhoneResponseLiveData: MutableLiveData<IsNewUserWithHashModel> = MutableLiveData()
    val resendPhoneLiveData: MutableLiveData<ResendCodeModel> = MutableLiveData(ResendCodeModel())
    val validateCodeLiveData: MutableLiveData<ValidationCodeModel> = MutableLiveData()

    val otp: MutableLiveData<String> = MutableLiveData("")

    var phone: String? = null
    var timeStamp: Long? = null

    fun onCloseClick() = viewModelScope.launch {
        navigationManager.popBackStack()
    }

    fun sendPhone(phone: String) {
        // TODO: migrate to channel to remove this hack
        failure.call()
        this.phone = phone

        viewModelScope.launch(Dispatchers.IO) {
            registrationRepository.sendPhone(SendPhoneRequestModel(phone))
                .either(failure::postValue) {
                    sendPhoneResponseLiveData.postValue(it)

                    launch { navigationManager.navigate(ShowOtpInputScreen()) }
                }
        }
    }

    fun resendCode() {
        val hash = sendPhoneResponseLiveData.value?.hash.orEmpty()
        viewModelScope.launch(Dispatchers.IO) {
            registrationRepository.resendCode(ResendSmsRequestModel(hash))
                .either(failure::postValue, resendPhoneLiveData::postValue)
        }
    }

    fun validateCode(code: String) {
        val phone = this.phone!!
        val hash = sendPhoneResponseLiveData.value?.hash.orEmpty()
        val isNew = sendPhoneResponseLiveData.value?.isNew ?: true
        viewModelScope.launch(Dispatchers.IO) {
            registrationRepository.validateOtp(
                ValidateSmsCodeRequestModel(
                    phone = phone,
                    hash = hash,
                    isNew = isNew,
                    otpCode = code
                )
            ).either(failure::postValue) { responseModel ->
                if (responseModel.success) {
                    appPreferences.apply {
                        token = responseModel.token
                        secret = responseModel.secret
                    }

                    launch {
                        if (isNew) {
                            navigationManager.popBackStack(ShowAuthorizationScreen())
                            navigationManager.navigate(ShowUserDataInputScreen())
                        } else {
                            navigationManager.popBackStack(ShowOnBoarding())
                            navigationManager.navigate(ShowMainScreenRoute())
                        }
                    }
                }
                validateCodeLiveData.postValue(responseModel)
            }
        }
    }

    fun saveUserData(name: String, surname: String, email: String) = viewModelScope.launch {
        navigationManager.navigate(ShowSuccessScreen())
    }
}