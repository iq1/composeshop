package com.dodin.composeshop.presentation.product

import com.dodin.composeshop.core.base.Route

class ShowProductRoute(private val id: Int) : Route {
    override val destination: String
        get() = route.replace("{id}", id.toString())

    companion object {
        const val route = "product/{id}"
    }
}