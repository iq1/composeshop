package com.dodin.composeshop.presentation.main

import androidx.compose.foundation.layout.Box
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.unit.sp

@Composable
fun StubScreen(name: String) {
    Box(contentAlignment = Alignment.Center) {
        Text(name, fontSize = 28.sp)
    }
}