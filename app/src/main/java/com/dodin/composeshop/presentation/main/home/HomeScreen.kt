package com.dodin.composeshop.presentation.main.home

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.dodin.composeshop.R
import com.dodin.composeshop.presentation.models.blogs.BlogModel
import com.dodin.composeshop.presentation.models.categories.CategoryModel
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.dodin.composeshop.theme.ComposeShopTheme
import com.dodin.composeshop.theme.Gray900
import com.dodin.composeshop.theme.Gray950
import com.dodin.composeshop.theme.GrayRampLight

@Composable
fun HomeScreen(
    viewModel: HomeViewModel,
    scrollState: ScrollState,
    blogScrollState: LazyListState,
    newScrollState: LazyListState,
) {
    val banners = viewModel.bannersLiveData.observeAsState()
    val newProducts = viewModel.newProductsLiveData.observeAsState()
    val hitProducts = viewModel.hitProductsLiveData.observeAsState()
    val categories = viewModel.categoriesLiveData.observeAsState()
    val blogPosts  = viewModel.blogsLiveData.observeAsState()

    HomeScreenContent(
        banners.value?.banners ?: emptyList(),
        categories.value ?: emptyList(),
        newProducts.value ?: emptyList(),
        hitProducts.value ?: emptyList(),
        blogPosts.value ?: emptyList(),
        scrollState,
        blogScrollState,
        newScrollState,
        viewModel::onSearchClick,
        viewModel::onCategoryClick,
        viewModel::onClickProduct,
        viewModel::onFavoriteClick,
        viewModel::onCartClick,
        viewModel::onCategoriesClick,
        viewModel::onBlogPostClick,
    )
}

@Composable
fun HomeScreenContent(
    banners: List<String>,
    categories: List<CategoryModel>,
    newProducts: List<ProductModel>,
    hitProducts: List<ProductModel>,
    blogPosts: List<BlogModel>,
    scrollState: ScrollState,
    blogScrollState: LazyListState,
    newScrollState: LazyListState,
    onSearchClick: () -> Unit,
    onCategoryClick: (CategoryModel) -> Unit,
    onClickProduct: (ProductModel) -> Unit,
    onFavoriteClick: (ProductModel) -> Unit,
    onCartClick: (ProductModel) -> Unit,
    onCategoriesClick: () -> Unit,
    onBlogPostClick: (BlogModel) -> Unit,
) {
    Column {
        TopBar(onSearchClick)
        Column(
            modifier = Modifier
                .padding(bottom = 24.dp)
                .fillMaxWidth()
                .fillMaxHeight()
                .verticalScroll(scrollState)
        ) {
            BannersCarousel(
                banners,
                modifier = Modifier.padding(top = 12.dp, start = 8.dp, end = 8.dp)
            )
            CategoriesList(categories, onCategoryClick)

            Text(
                text = stringResource(R.string.new_products),
                color = Gray900,
                style = MaterialTheme.typography.h2,
                modifier = Modifier.padding(top = 50.dp, start = 24.dp, end = 24.dp)
            )
            NewProductsList(
                newProducts,
                onClickProduct,
                onFavoriteClick,
                onCartClick,
                newScrollState
            )

            Text(
                text = stringResource(R.string.hit_products),
                color = Gray900,
                style = MaterialTheme.typography.h2,
                modifier = Modifier.padding(top = 50.dp, start = 24.dp, end = 24.dp)
            )
            HitProductsList(
                hitProducts,
                onClickProduct,
                onFavoriteClick,
                onCartClick
            )

            OutlinedButton(
                shape = MaterialTheme.shapes.medium,
                modifier = Modifier
                    .padding(top = 30.dp, bottom = 24.dp, start = 24.dp, end = 24.dp)
                    .height(60.dp)
                    .fillMaxWidth(),
                onClick = onCategoriesClick
            ) {
                Text(text = stringResource(R.string.home_go_to_listing), color = Gray950)
            }

            BlogPostsList(
                list = blogPosts,
                blogScrollState = blogScrollState,
                onClick = onBlogPostClick,
            )
        }
    }
}

@Composable
fun TopBar(
    onSearchClick: () -> Unit,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Spacer(modifier = Modifier.weight(1f))
        Image(
            painter = painterResource(R.drawable.custom_shop_onboarding_logo_placeholder),
            contentDescription = null,
            modifier = Modifier.size(24.dp)
        )
        Spacer(modifier = Modifier.weight(1f))
        IconButton(onClick = onSearchClick) {
            Icon(Icons.Default.Search, contentDescription = null)
        }
    }
}

@Composable
fun CategoriesList(
    categories: List<CategoryModel>,
    onCategoryClick: (CategoryModel) -> Unit,
) {
    LazyRow(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 40.dp),
        contentPadding = PaddingValues(horizontal = 8.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        items(categories) { category ->
            Card(
                backgroundColor = GrayRampLight,
                modifier = Modifier
                    .size(135.dp, 150.dp)
                    .clickable { onCategoryClick(category) }
            ) {
                Box(modifier = Modifier.fillMaxSize()) {
                    Image(
                        painter = rememberImagePainter(category.image),
                        modifier = Modifier.height(100.dp),
                        contentScale = ContentScale.Crop,
                        contentDescription = category.name,
                        alignment = Alignment.TopStart,
                    )
                    Text(
                        text = category.name,
                        fontSize = 15.sp,
                        maxLines = 2,
                        modifier = Modifier
                            .align(Alignment.BottomStart)
                            .padding(bottom = 11.dp, start = 15.dp, end = 8.dp)
                    )
                }
            }
        }
    }
}

@Composable
fun BlogPostsList(list: List<BlogModel>, blogScrollState: LazyListState, onClick: (BlogModel) -> Unit) {
    if (list.isEmpty()) return

    Text(
        text = stringResource(R.string.read_our_blog),
        color = Gray900,
        style = MaterialTheme.typography.h2,
        modifier = Modifier.padding(top = 50.dp, start = 24.dp, end = 24.dp)
    )

    LazyRow(
        state = blogScrollState,
        modifier = Modifier.padding(top = 25.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        contentPadding = PaddingValues(horizontal = 24.dp),
    ) {
        items(list) { post ->
            Column(modifier = Modifier.clickable { onClick(post) }) {
                Image(
                    painter = rememberImagePainter(data = post.image),
                    contentScale = ContentScale.Crop,
                    alignment = Alignment.Center,
                    contentDescription = post.title,
                    modifier = Modifier
                        .size(265.dp, 146.dp)
                        .clip(MaterialTheme.shapes.medium)
                )
                Text(
                    text = post.title.orEmpty(),
                    maxLines = 2,
                    fontSize = 15.sp,
                    modifier = Modifier.padding(top = 12.dp),
                    color = Gray900,
                )
            }
        }
    }
}

@Preview
@Composable
fun HomeScreenPreview() {
    ComposeShopTheme {
        HomeScreenContent(
            mockBanners,
            mockCategories,
            mockNewProducts,
            mockPopularProducts,
            emptyList(),
            rememberScrollState(),
            rememberLazyListState(),
            rememberLazyListState(),
            {},
            {},
            {},
            {},
            {},
            {},
            {}
        )
    }
}