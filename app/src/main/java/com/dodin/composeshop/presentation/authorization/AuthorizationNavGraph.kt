package com.dodin.composeshop.presentation.authorization

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.dodin.composeshop.presentation.main.ShowMainScreenRoute
import com.dodin.composeshop.presentation.onboarding.ShowOnBoarding
import com.dodin.composeshop.tools.getViewModel

fun NavGraphBuilder.loginGraph(navController: NavController) {
    navigation(startDestination = ShowPhoneScreen.route, route = ShowAuthorizationScreen.route) {
        composable(ShowPhoneScreen.route) {
            val viewModel = getViewModel<UserAuthorizationViewModel>(navController)
            PhoneInputScreen(viewModel)
        }
        composable(ShowOtpInputScreen.route) {
            val viewModel = getViewModel<UserAuthorizationViewModel>(navController, ShowPhoneScreen.route)
            OtpInputScreen(viewModel)
        }
        composable(ShowUserDataInputScreen.route) {
            val viewModel = getViewModel<UserAuthorizationViewModel>(navController)
            UserDataInputScreen(viewModel)
        }
        composable(ShowSuccessScreen.route) {
            // Example of navigation using regular NavController instead of NavigationManager commands
            RegistrationSuccessfulScreen {
                navController.navigate(ShowMainScreenRoute.route) {
                    popUpTo(ShowOnBoarding.route) {
                        inclusive = true
                    }
                }
            }
        }
    }
}