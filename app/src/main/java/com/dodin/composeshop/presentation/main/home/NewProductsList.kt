package com.dodin.composeshop.presentation.main.home

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.dodin.composeshop.components.ProductVertical
import com.dodin.composeshop.components.VerticalDivider
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.dodin.composeshop.theme.GrayRampLightenest

@Composable
fun NewProductsList(
    newProducts: List<ProductModel>,
    onProductClick: (ProductModel) -> Unit,
    onFavoriteClick: (ProductModel) -> Unit,
    onCartClick: (ProductModel) -> Unit,
    newScrollState: LazyListState
) {
    LazyRow(
        state = newScrollState,
        modifier = Modifier
            .fillMaxWidth()
            .height(280.dp)
            .padding(top = 24.dp, start = 8.dp, end = 8.dp),
    ) {
        itemsIndexed(newProducts) { index, product ->
            ProductVertical(product, onProductClick, onFavoriteClick, onCartClick)

            if (index < newProducts.size - 1) VerticalDivider(
                color = GrayRampLightenest,
                thickness = 1.dp,
                modifier = Modifier.padding(horizontal = 4.dp)
            )
        }
    }
}

@Preview
@Composable
fun NewProductsListPreview() {
    NewProductsList(mockNewProducts, {}, {}, {}, rememberLazyListState())
}