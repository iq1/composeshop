package com.dodin.composeshop.presentation.models.banners

import com.dodin.composeshop.core.base.converters.NetworkConverter
import com.dodin.composeshop.data.network.models.banners.MainScreenBannersNetworkModel

data class BannersModel(val banners: List<String>) {
    companion object : NetworkConverter<MainScreenBannersNetworkModel, BannersModel> {
        override fun convertFromNetworkResponse(response: MainScreenBannersNetworkModel): BannersModel {
            return BannersModel(response.banners.map { it.url })
        }
    }
}