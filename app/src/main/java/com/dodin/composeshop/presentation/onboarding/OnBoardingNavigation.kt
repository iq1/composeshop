package com.dodin.composeshop.presentation.onboarding

import com.dodin.composeshop.core.base.Route

class ShowOnBoarding : Route {
    override val destination: String
        get() = route

    companion object {
        const val route = "onboarding"
    }
}