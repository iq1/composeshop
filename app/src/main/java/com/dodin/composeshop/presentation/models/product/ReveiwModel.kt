package com.dodin.composeshop.presentation.models.product

data class ReviewModel(
    val name: String,
    val content: String,
    val value: Int,
    val date: String,
)