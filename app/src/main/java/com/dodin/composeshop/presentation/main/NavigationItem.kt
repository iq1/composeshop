package com.dodin.composeshop.presentation.main

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class NavigationItem(
    val route: String,
    @StringRes val titleResId: Int,
    val icon: IconResource,
    val badge: BadgeItem = BadgeItem.None,
)

sealed class IconResource {
    data class Resource(@DrawableRes val drawableResId: Int) : IconResource()
    data class Downloadable(val url: String) : IconResource()
    // TODO: add compose vector
}

sealed class BadgeItem {
    object None : BadgeItem()
    object Dot : BadgeItem()
    data class Number(val count: Int) : BadgeItem()
}