package com.dodin.composeshop.presentation.main.home

import com.dodin.composeshop.presentation.models.categories.CategoryModel
import com.dodin.composeshop.presentation.models.product.ProductAttributeModel
import com.dodin.composeshop.presentation.models.product.ProductLabelModel
import com.dodin.composeshop.presentation.models.product.ProductModel
import java.math.BigDecimal

val mockBanners = listOf(
    "https://shop-app.m2.branderstudio.com/media/mobile/main_screen/banners/Frame_159.png",
    "https://shop-app.m2.branderstudio.com/media/mobile/main_screen/banners/imgonline-com-ua-Resize-tYtsX9DeugF5D.jpg",
    "https://shop-app.m2.branderstudio.com/media/mobile/main_screen/banners/imgonline-com-ua-Resize-IuJRhqshaoSKw3qr.jpg",
)

val mockCategories = listOf(
    CategoryModel(
        id = 14,
        key = "1",
        image = "https://shop-app.m2.branderstudio.com/media/catalog/category/file_8.jpg",
        mobileImage = "https://shop-app.m2.branderstudio.com/media/catalog/category/file_8.jpg",
        name = "Вино",
        position = 0,
        level = 1
    ),
    CategoryModel(
        id = 15,
        key = "1",
        image = "https://shop-app.m2.branderstudio.com/media/catalog/category/file_15.jpg",
        mobileImage = "https://shop-app.m2.branderstudio.com/media/catalog/category/file_15.jpg",
        name = "Игристое вино",
        position = 0,
        level = 1
    ),
    CategoryModel(
        id = 16,
        key = "1",
        image = "https://shop-app.m2.branderstudio.com/media/catalog/category/file_13.jpg",
        mobileImage = "https://shop-app.m2.branderstudio.com/media/catalog/category/file_13.jpg",
        name = "Крепкий алкоголь",
        position = 0,
        level = 1
    ),
    CategoryModel(
        id = 35,
        key = "1",
        image = "https://shop-app.m2.branderstudio.com/media/catalog/category/file_3.jpeg",
        mobileImage = "https://shop-app.m2.branderstudio.com/media/catalog/category/file_3.jpeg",
        name = "Пиво",
        position = 0,
        level = 1
    ),
)

val products = listOf(
    ProductModel(
        id = 27,
        sku = "UZ62582",
        image = "https://shop-app.m2.branderstudio.com/media/catalog/product/cache/1af772ec702cef34c3cd1909e95b1f2d/i/m/imgonline-com-ua-resize-v7yjovjigdtc.jpg",
        name = "Cava Carta Nevada Freixenet Cava Carta Nevada Freixenet Cava Carta Nevada Freixenet Cava Carta Nevada Freixenet Cava Carta Nevada Freixenet",
        description = "<p>Cava Carta Nevada Freixenet - игристый розовый брют из винограда сорта Трепат. Собранный при помощи машин урожай тщательно очищается от гребней и охлаждается. Винификация начинается с мацерации, которая длится 12 часов. Затем ягоды прессуются, полученное сусло ферментируется в течение 15 дней при температуре в 10°C. Снятое с осадка вино фильтруется, подвергается стабилизации и вновь процеживается. Вторичная ферментация вина начинается после бутилирования и длится 9-12 месяцев, после чего кава проходит процедуры дегоржажа и добавления дозажного ликера.</p>",
        price = BigDecimal(320),
        specialPrice = BigDecimal(200),
        currency = "грн",
        labels = listOf(ProductLabelModel("Хит", "#ff0000")),
        isFavorite = true,
        isAvailable = false,
        cartCount = 1,
        ratingCount = 2,
        rating = 100,
        attributes = listOf(
            ProductAttributeModel(
                title = "Цвет",
                value = "Красный",
            ),
            ProductAttributeModel(
                title = "Крепость",
                value = "20%",
            ),
        ),
        configurableOptions = null,
        variants = null,
    ),
    ProductModel(
        id = 49,
        sku = "FD932044",
        image = "https://shop-app.m2.branderstudio.com/media/catalog/product/cache/1af772ec702cef34c3cd1909e95b1f2d/i/m/imgonline-com-ua-resize-v7yjovjigdtc.jpg",
        name = "Canti Prosecco Millesimato",
        price = BigDecimal(220),
        currency = "грн",
        labels = listOf(ProductLabelModel("Хит", "#ff0000")),
        configurableOptions = null,
        variants = null,
    ),
    ProductModel(
        id = 40,
        sku = "ВА878431",
        image = "https://shop-app.m2.branderstudio.com/media/catalog/product/cache/1af772ec702cef34c3cd1909e95b1f2d/i/m/imgonline-com-ua-resize-wkj9ul3qbvpteq.jpg",
        name = "Torres Vina Esmeralda",
        price = BigDecimal(900),
        currency = "грн",
        labels = listOf(ProductLabelModel("Хит", "#ff0000")),
        configurableOptions = null,
        variants = null,
    ),
    ProductModel(
        id = 33,
        sku = "VC64531",
        image = "https://shop-app.m2.branderstudio.com/media/catalog/product/cache/1af772ec702cef34c3cd1909e95b1f2d/_/r/_rectangle_54.png",
        name = "Chardonnay Cycles Gladiator",
        price = BigDecimal(220),
        currency = "грн",
        labels = listOf(ProductLabelModel("Хит", "#ff0000"), ProductLabelModel("Sale", "#00ff00")),
        configurableOptions = null,
        variants = null,
    ),
)

val mockNewProducts = products.take(5)
val mockPopularProducts = products.shuffled().take(5)