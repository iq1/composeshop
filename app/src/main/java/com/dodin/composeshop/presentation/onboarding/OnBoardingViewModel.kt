package com.dodin.composeshop.presentation.onboarding

import androidx.lifecycle.viewModelScope
import com.dodin.composeshop.core.base.NavigationManager
import com.dodin.composeshop.presentation.authorization.ShowAuthorizationScreen
import com.dodin.composeshop.presentation.main.ShowMainScreenRoute
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ua.brander.core.viewmodel.BaseViewModel

class OnBoardingViewModel : BaseViewModel(), KoinComponent {
    private val navigationManager: NavigationManager by inject()

    fun onSkipClick() = viewModelScope.launch {
        navigationManager.popBackStack(ShowOnBoarding())
        navigationManager.navigate(ShowMainScreenRoute())
    }

    fun onAuthClick() = viewModelScope.launch {
        navigationManager.navigate(ShowAuthorizationScreen())
    }
}