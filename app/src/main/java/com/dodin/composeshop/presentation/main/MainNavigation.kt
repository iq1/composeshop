package com.dodin.composeshop.presentation.main

import com.dodin.composeshop.core.base.Route

class ShowMainScreenRoute : Route {
    override val destination: String
        get() = route

    companion object {
        const val route = "main"
    }
}