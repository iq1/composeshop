package com.dodin.composeshop.presentation.authorization

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.dodin.composeshop.R
import com.dodin.composeshop.components.ShopAppSecondaryButton

@Composable
fun RegistrationSuccessfulScreen(
    onButtonClick: () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxWidth()
            .padding(horizontal = 24.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Image(
            painter = painterResource(R.drawable.custom_shop_registration_successful_icon),
            contentDescription = null,
            modifier = Modifier.padding(top = 90.dp)
        )

        Text(
            stringResource(R.string.successful_string),
            fontSize = 28.sp,
            modifier = Modifier.padding(top = 50.dp)
        )
        Text(
            stringResource(R.string.thanks_for_registration_text),
            fontSize = 15.sp,
            textAlign = TextAlign.Center
        )

        Spacer(Modifier.weight(1f))
        ShopAppSecondaryButton(
            text = stringResource(R.string.to_the_main_screen_text),
            onClick = onButtonClick,
            modifier = Modifier.padding(bottom = 50.dp).fillMaxWidth()
        )
    }
}

@Preview
@Composable
fun RegistrationSuccessfulScreenPreview() {
    RegistrationSuccessfulScreen {}
}