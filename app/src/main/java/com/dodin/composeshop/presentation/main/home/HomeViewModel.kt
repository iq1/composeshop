package com.dodin.composeshop.presentation.main.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.dodin.composeshop.Constants.WEBVIEW_MOBILE_PARAM
import com.dodin.composeshop.core.base.NavigationManager
import com.dodin.composeshop.data.network.repository.HomeScreenRepository
import com.dodin.composeshop.presentation.models.banners.BannersModel
import com.dodin.composeshop.presentation.models.blogs.BlogModel
import com.dodin.composeshop.presentation.models.categories.CategoryModel
import com.dodin.composeshop.presentation.models.product.ProductModel
import com.dodin.composeshop.presentation.models.screens.HomeScreenModel
import com.dodin.composeshop.presentation.product.ShowProductRoute
import com.dodin.composeshop.presentation.webview.ShowWebViewScreen
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ua.brander.core.viewmodel.BaseViewModel

class HomeViewModel : BaseViewModel(), KoinComponent {
    private val navigationManager: NavigationManager by inject()
    private val homeScreenRepository: HomeScreenRepository by inject()

    val bannersLiveData: MutableLiveData<BannersModel> = MutableLiveData()
    val categoriesLiveData: MutableLiveData<List<CategoryModel>> = MutableLiveData()
    val newProductsLiveData: MutableLiveData<List<ProductModel>> = MutableLiveData()
    val hitProductsLiveData: MutableLiveData<List<ProductModel>> = MutableLiveData()
    val blogsLiveData: MutableLiveData<List<BlogModel>> = MutableLiveData()

    init {
        loadData()
    }

    fun onClickProduct(product: ProductModel) = viewModelScope.launch {
        navigationManager.navigate(ShowProductRoute(product.id))
    }

    fun onSearchClick() {

    }

    fun onCategoriesClick() {

    }

    fun onCategoryClick(category: CategoryModel) {

    }

    fun onFavoriteClick(product: ProductModel) {

    }

    fun onCartClick(product: ProductModel) {

    }

    fun onBlogPostClick(post: BlogModel) = viewModelScope.launch {
        navigationManager.navigate(
            ShowWebViewScreen(
                post.fullUrl + WEBVIEW_MOBILE_PARAM,
                post.title.orEmpty()
            )
        )
    }

    private fun loadData() {
        viewModelScope.launch(Dispatchers.IO) {
            homeScreenRepository.getData().either(failure::postValue, ::handleData)
        }
    }

    private fun handleData(homeScreenModel: HomeScreenModel) {
        bannersLiveData.postValue(homeScreenModel.banners)
        categoriesLiveData.postValue(homeScreenModel.categories)
        newProductsLiveData.postValue(homeScreenModel.newProducts)
        hitProductsLiveData.postValue(homeScreenModel.hitProducts)
        blogsLiveData.postValue(homeScreenModel.blogs)
    }
}