package com.dodin.composeshop.presentation.models

import com.dodin.composeshop.core.base.converters.NetworkConverter
import com.dodin.composeshop.data.network.models.registration.SendPhoneResponseModel

data class IsNewUserWithHashModel(val isNew: Boolean, val hash: String) {

    companion object : NetworkConverter<SendPhoneResponseModel, IsNewUserWithHashModel> {
        override fun convertFromNetworkResponse(response: SendPhoneResponseModel): IsNewUserWithHashModel {
            return IsNewUserWithHashModel(response.isNew, response.hash)
        }
    }
}